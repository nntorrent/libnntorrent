#include "tracker.h"

#if defined(__FreeBSD__)
#include <sys/endian.h> // endian-ness conversion
#else
#include <endian.h>
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h> // sockaddr_in
#include <arpa/inet.h> // htons() and inet_pton()
#include <netdb.h> // getaddrinfo
#include <time.h>
#include <syslog.h>
#include <stdbool.h>
#include <openssl/sha.h>
#include <unistd.h>
#include <inttypes.h>

#include <bencode.h>

#include "util.h"

int tracker_comm(tracker_t *);
int tracker_comm_udp(tracker_t *);
int tracker_comm_tcp(tracker_t *);

int tracker_send_udp(tracker_t *);
int tracker_recv_udp(tracker_t *);

size_t tracker_recv_tcp_callback(void *, size_t, size_t, void *);
char *tracker_build_url_params(tracker_t *);

tracker_t *
tracker_new()
{
  tracker_t *res = malloc(sizeof(tracker_t));

  res->curlh = NULL;
  res->sock = -1;

  res->url = NULL;
  res->hostname = NULL;
  res->port = -1;
  res->protocol = TRACKER_PROTOCOL_UNSUPPORTED;

  res->req = tracker_request_new();
  res->last_response = tracker_response_new();

  res->connection_id = -1;

  return res;
}

int
tracker_init(tracker_t *res, torrent_info_t *ti, int port)
{
  /*
   * socket
   */

  res->curlh = curl_easy_init();

  /*sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);*/
  res->sock = socket(AF_INET, SOCK_DGRAM, 17);

  struct sockaddr_in sa;
  sa.sin_family = AF_INET;
  sa.sin_port = htons(port);
  sa.sin_addr.s_addr = INADDR_ANY;
  /*inet_pton(AF_INET, "0.0.0.0", &sa.sin_addr);*/

  /* timeout for receiving packets */
  struct timeval tv;
  tv.tv_sec = 10;
  tv.tv_usec = 0;
  if (setsockopt(res->sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
    syslog(LOG_DEBUG, "tracker_new: could not set timeout on tracker sock: %m");
  }

  if (bind(res->sock, (struct sockaddr *) &sa, sizeof(sa)) != 0) {
    syslog(LOG_DEBUG, "tracker_new: could not bind tracker sock: %m");
    return -1;
  }

  /*
   * info hash
   */

  strncpy((char *) res->req->info_hash, (char *) ti->info_hash, SHA_DIGEST_LENGTH);

  /*
   * peer id
   */

  /* https://wiki.theory.org/BitTorrentSpecification#peer_id */
  memcpy(res->req->peer_id, "-NN0001-", 8);
  uint8_t t;
  for (size_t i = 8; i < PEER_ID_SIZE; i++) {
    t = rand() % 10 + 48; // random ascii numbers
    memcpy(res->req->peer_id + i, &t, sizeof(uint8_t));
  }

  return 0;
}

int
tracker_set_host(tracker_t *tr, char *hostname, uint16_t port, tracker_protocol_t proto)
{
  if (tr->hostname)
    free(tr->hostname);

  /* hostname */
  tr->hostname = malloc(strlen(hostname) + 1);
  memcpy(tr->hostname, hostname, strlen(hostname)+1);
  tr->hostname[strlen(hostname)] = '\0';

  /* port */
  tr->port = port;

  /* protocol */
  tr->protocol = proto;

  return 0;
}

int
tracker_set_url(tracker_t *tr, char *url) {
  if (tr->url)
    free(tr->url);

  tr->url = malloc(strlen(url) + 1);
  memcpy(tr->url, url, strlen(url));
  tr->url[strlen(url)] = '\0';

  return 0;
}

void
tracker_free(tracker_t *tr)
{
  if (!tr) return;

  if (tr->hostname)
    free(tr->hostname);

  if (tr->url)
    free(tr->url);

  tracker_request_free(tr->req);
  tracker_response_free(tr->last_response);
  close(tr->sock);
  curl_easy_cleanup(tr->curlh);

  free(tr);
}

int
tracker_connect(tracker_t *tr)
{
  if (tr->protocol == TRACKER_PROTOCOL_HTTP ||
      tr->protocol == TRACKER_PROTOCOL_HTTPS) {
    syslog(LOG_DEBUG, "skipping tracker connect: http(s) trackers do not require connection");
    return 0;
  }

  /* bittorrent udp spec: http://bittorrent.org/beps/bep_0015.html */
  /* connection_id: int64 0x41727101980 */
  tr->req->action = TRACKER_ACTION_CONNECT;
  tr->req->transaction_id = gen_rand_32();

  if (tracker_comm(tr) != 0) {
    return -1;
  }

  tr->req->connection_id = tr->last_response->connection_id;

  return 0;
}

int
tracker_comm_udp(tracker_t *tr)
{
  if (tracker_send_udp(tr) != 0) {
    syslog(LOG_DEBUG, "tracker_connect: could not send request");
    return -1;
  }

  if (tracker_recv_udp(tr) != 0) {
    syslog(LOG_DEBUG, "tracker_connect: could not recv result");
    return -1;
  }

  if (!(tr->last_response->action == tr->req->action &&
        tr->last_response->transaction_id == tr->req->transaction_id)) {
    syslog(LOG_DEBUG, "tracker_connect: action and/or transaction_id don't match");
    return -1;
  }

  return 0;
}

int
tracker_recv_udp(tracker_t *tr)
{
  struct addrinfo hints;
  struct addrinfo *res;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;

  char port[8];
  snprintf(port, 8, "%d", tr->port);

  int ret;
  if ((ret = getaddrinfo(tr->hostname, port, &hints, &res)) != 0) {
    syslog(LOG_DEBUG, "tracker_recv: could not get address info: %s", gai_strerror(ret));
    return -1;
  }

  socklen_t *slen = malloc(sizeof(socklen_t));
  *slen = sizeof(res->ai_addr);

  size_t resp_buf_size = 20 + 8 * tr->req->numwant;
  char *resp = malloc(resp_buf_size);

  ssize_t len = recvfrom(tr->sock, resp, resp_buf_size, 0, res->ai_addr, slen);
  if (len <= 0) {
    syslog(LOG_DEBUG, "tracker_recv: could not recv: %m");
    free(resp);
    free(slen);
    return -1;
  }

  tracker_response_free(tr->last_response);
  tr->last_response = tracker_response_new();
  tracker_response_parse(tr->last_response, resp, len);

  free(slen);
  free(resp);
  freeaddrinfo(res);

  return 0;
}

int
tracker_send_udp(tracker_t *tr)
{
  struct addrinfo hints;
  struct addrinfo *res;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_DGRAM;

  char port[8];
  snprintf(port, 8, "%d", tr->port);

  int ret;
  if ((ret = getaddrinfo(tr->hostname, port, &hints, &res)) != 0) {
    syslog(LOG_DEBUG, "tracker_send_udp: could not get address info: %s", gai_strerror(ret));
    return -1;
  }

  ssize_t msglen;
  char *msg = tracker_request_serialize(tr->req, &msglen);

  ssize_t len = sendto(tr->sock, msg, msglen, 0, res->ai_addr, res->ai_addrlen);
  if (len != msglen) {
    syslog(LOG_DEBUG, "tracker_send_udp: could not send: %m");
    free(msg);
    return -1;
  }

  freeaddrinfo(res);
  free(msg);

  return 0;
}

int
tracker_announce(tracker_t *tr)
{
  syslog(LOG_DEBUG, "announcing to tracker %s", tr->hostname);
  tr->req->action = TRACKER_ACTION_ANNOUNCE;
  tr->req->transaction_id = gen_rand_32();

  if (tracker_comm(tr) != 0) {
    return -1;
  }

  syslog(LOG_DEBUG, "interval = %ds = %dmin; leechers = %d; seeders = %d",
         tr->last_response->interval, tr->last_response->interval / 60,
         tr->last_response->leechers,
         tr->last_response->seeders);

  return 0;
}

int
tracker_comm_tcp(tracker_t *tr)
{
  char *url = tracker_build_url_params(tr);
  curl_easy_setopt(tr->curlh, CURLOPT_URL, url);
  curl_easy_setopt(tr->curlh, CURLOPT_WRITEFUNCTION, tracker_recv_tcp_callback);
  curl_easy_setopt(tr->curlh, CURLOPT_WRITEDATA, (void *) tr);
  curl_easy_setopt(tr->curlh, CURLOPT_VERBOSE, 1);
  curl_easy_setopt(tr->curlh, CURLOPT_TIMEOUT, 10L);
  curl_easy_setopt(tr->curlh, CURLOPT_NOSIGNAL, 1);

  char buf[CURL_ERROR_SIZE];
  memset(buf, 0, CURL_ERROR_SIZE);
  curl_easy_setopt(tr->curlh, CURLOPT_ERRORBUFFER, buf);

  CURLcode ret;
  if ((ret = curl_easy_perform(tr->curlh)) != CURLE_OK) {
    syslog(LOG_INFO, "could not connect to tracker: libcurl error (%d): %s", ret, buf);
    curl_easy_setopt(tr->curlh, CURLOPT_ERRORBUFFER, NULL);
    return -1;
  }

  curl_easy_setopt(tr->curlh, CURLOPT_ERRORBUFFER, NULL);
  return 0;
}

size_t
tracker_recv_tcp_callback(void *buf, size_t size, size_t nmemb, void *t)
{
  size_t realsize = size * nmemb;
  tracker_t *tr = (tracker_t *) t;
  if (tr->last_response)
    tracker_response_free(tr->last_response);
  tr->last_response = tracker_response_new();

  bencode_t *ben = bencode_new();

  if (bencode_decode(buf, realsize, ben) != 0 ||
      ben->type != BENCODE_DICT) {
    syslog(LOG_DEBUG, "could not parse tracker message: decoding failed");
    bencode_free(ben);
    return -1;
  }

  /*
   * interval
   */
  bencode_t *b_interval = bencode_new();
  if (bencode_dict_get(ben, "interval", b_interval) != 0 ||
      b_interval->type != BENCODE_INT) {
    syslog(LOG_DEBUG, "could not parse tracker message: 'interval' not an integer");
    bencode_free(b_interval);
    bencode_free(ben);
    return -1;
  }

  tr->last_response->interval = b_interval->value.i;
  bencode_free(b_interval);

  /*
   * seeders
   */
  bencode_t *b_seeders = bencode_new();
  if (bencode_dict_get(ben, "complete", b_seeders) != 0 ||
      b_seeders->type != BENCODE_INT) {
    syslog(LOG_DEBUG, "could not parse tracker message: 'complete' not an integer");
    bencode_free(b_seeders);
    bencode_free(ben);
    return -1;
  }

  tr->last_response->seeders = b_seeders->value.i;
  bencode_free(b_seeders);

  /*
   * leechers
   */
  bencode_t *b_leechers = bencode_new();
  if (bencode_dict_get(ben, "incomplete", b_leechers) != 0 ||
      b_leechers->type != BENCODE_INT) {
    syslog(LOG_DEBUG, "could not parse tracker message: 'incomplete' not an integer");
    bencode_free(b_leechers);
    bencode_free(ben);
    return -1;
  }

  tr->last_response->leechers = b_leechers->value.i;
  bencode_free(b_leechers);

  /*
   * peers
   */
  bencode_t *b_peers = bencode_new();
  if (bencode_dict_get(ben, "peers", b_peers) != 0 ||
      b_peers->type != BENCODE_STR) {
    syslog(LOG_DEBUG, "could not parse tracker message: peers not a string");
    bencode_free(b_peers);
    bencode_free(ben);
    return -1;
  }

  tr->last_response->peers_len = b_peers->length / 6;
  tr->last_response->peers = malloc(sizeof(tracker_peer_t) * tr->last_response->peers_len);

  for (int i = 0; i < tr->last_response->peers_len; i++) {
    tracker_peer_t p;
    memcpy(&p.ipa,  b_peers->value.s+(6 * i), sizeof(p.ipa));
    memcpy(&p.port, b_peers->value.s+(4 + 6 * i), sizeof(p.port));

    tr->last_response->peers[i] = (tracker_peer_t) {.ipa = p.ipa, .port = be16toh(p.port)};
  }

  bencode_free(b_peers);

  return realsize;
}

int
tracker_comm(tracker_t *tr)
{
  switch (tr->protocol) {
    case TRACKER_PROTOCOL_UDP:
      return tracker_comm_udp(tr);

    case TRACKER_PROTOCOL_HTTP:
    case TRACKER_PROTOCOL_HTTPS:
      return tracker_comm_tcp(tr);

    default:
      return -1;
  }
}

char *
tracker_build_url_params(tracker_t *tr)
{
  char *params = malloc(2048);
  memset(params, 0, 2048);

  char *info_hash = curl_easy_escape(tr->curlh, (char *) tr->req->info_hash, INFO_HASH_SIZE);
  char *peer_id = curl_easy_escape(tr->curlh, (char *) tr->req->peer_id, PEER_ID_SIZE);

  char event[25];
  memset(event, 0, 25);
  switch (tr->req->event) {
    case TRACKER_EVENT_STARTED:
      strcpy(event, "started");
      break;

    case TRACKER_EVENT_STOPPED:
      strcpy(event, "stopped");
      break;

    case TRACKER_EVENT_COMPLETED:
      strcpy(event, "completed");
      break;

    default:
      break;
  }

  sprintf(params, "%s?info_hash=%s&peer_id=%s&port=%" SCNu16 "&uploaded=%lu&downloaded=%lu&left=%lu&compact=%d&no_peer_id=%d&event=%s&numwant=%u&key=%d",
          tr->url,
          info_hash,
          peer_id,
          (uint16_t) 9000,
          tr->req->uploaded,
          tr->req->downloaded,
          tr->req->left,
          1,
          1,
          event,
          tr->req->numwant,
          tr->req->key
  );

  free(info_hash);
  free(peer_id);

  return params;
}

