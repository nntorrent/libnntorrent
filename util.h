#ifndef UTIL_H
#define UTIL_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

void hex_print(char *, unsigned char *, size_t);
int32_t gen_rand_32();

#endif // UTIL_H
