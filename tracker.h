#ifndef TRACKER_H
#define TRACKER_H

/* UDP protocol: http://bittorrent.org/beps/bep_0015.html */

#include <stdint.h>
#include <curl/curl.h>

#include "metainfo.h"
#include "tracker_request.h"
#include "tracker_response.h"
#include "constants.h"

typedef struct {
  char *url;
  char *hostname;
  uint16_t port;
  tracker_protocol_t protocol;

  tracker_request_t *req;
  tracker_response_t *last_response;

  /* tcp */
  CURL *curlh;

  /* udp */
  int sock;
  int64_t connection_id;
} tracker_t;

tracker_t *tracker_new();
int tracker_init(tracker_t *, torrent_info_t *, int port);
int tracker_set_host(tracker_t *, char *hostname, uint16_t port, tracker_protocol_t);
int tracker_set_url(tracker_t *, char *);

int tracker_connect(tracker_t *);
int tracker_announce(tracker_t *);

void tracker_free(tracker_t *);

#endif // TRACKER_H
