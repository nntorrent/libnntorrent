#include "../../mainloop.h"

#include <sys/types.h>
#include <sys/event.h>
#include <errno.h>

int
ml_create()
{
  return kqueue();
}

void
ml_destroy(int loop)
{
  close(loop);
}

/* return positive for number of events, negative for error
 * input ml_event* for event output
 * input size to specify the maximum size of ml_event* and size is modified to specify
 * the actual size of output ml_event*
 */
int
ml_get_events(int loop, ml_event_t *events, int size)
{
  return kevent(loop, NULL, 0, events, size, 0);
}

/* returns positive for id, negative for error */
int
ml_create_timer(int loop, int id)
{
  return id;
}

int
ml_set_timer(int loop, int id, int timeout, bool oneshot)
{
  struct kevent ev;
  u_short flags;

  if (timeout <= 0) {
    flags = EV_DELETE;
  } else {
    flags = EV_ADD | EV_ENABLE;
    if (oneshot) {
      flags |= EV_ONESHOT;
    }
  }

  EV_SET(&ev, id, EVFILT_TIMER, flags, NOTE_SECONDS, timeout, NULL);
  return kevent(loop, &ev, 1, NULL, 0, NULL);
}

/* return 0 for success, negative for error */
int
ml_destroy_timer(int loop, int id)
{
  struct kevent ev;
  EV_SET(&ev, id, EVFILT_TIMER, EV_DELETE, 0, 0, NULL);
  return kevent(loop, &ev, 1, NULL, 0, NULL);
}

int
ml_create_custom(int loop, int id)
{
  struct kevent ev;
  EV_SET(&ev, id, EVFILT_USER, EV_ADD | EV_ENABLE, 0, 0, NULL);
  kevent(loop, &ev, 1, NULL, 0, NULL);
  return id;
}

int
ml_destroy_custom(int loop, int id)
{
  struct kevent ev;
  EV_SET(&ev, id, EVFILT_USER, EV_DELETE, 0, 0, NULL);
  kevent(loop, &ev, 1, NULL, 0, NULL);
  return id;
}

int
ml_notify_custom(int loop, int id)
{
  struct kevent ev;
  EV_SET(&ev, id, EVFILT_USER, EV_ENABLE, NOTE_TRIGGER, 0, NULL);
  return kevent(loop, &ev, 1, NULL, 0, NULL);
}

bool
ml_event_custom(ml_event_t *ev, int id)
{
  return (ev->filter == EVFILT_USER && ev->ident == id);
}

bool
ml_event_timer(ml_event_t *ev, int id)
{
  return (ev->filter == EVFILT_TIMER && ev->ident == id);
}

void *
ml_event_data(ml_event_t *ev)
{
  return ev->udata;
}

bool
ml_event_read(ml_event_t *ev)
{
  return ev->filter == EVFILT_READ;
}

bool
ml_event_write(ml_event_t *ev)
{
  return ev->filter == EVFILT_WRITE;
}

bool
ml_event_eof(ml_event_t *ev)
{
  return ev->filter == EVFILT_READ && ev->flags & EV_EOF;
}

int
ml_event_fd(ml_event_t *ev)
{
  return ev->ident;
}

int
ml_add_fd(int loop, int fd, enum ml_fd_event rw, void *data)
{
  struct kevent ev;
  EV_SET(&ev, fd, rw == ML_READ ? EVFILT_READ : EVFILT_WRITE, EV_ADD | EV_ENABLE, 0, 0, data);
  return kevent(loop, &ev, 1, NULL, 0, NULL);
}

int
ml_del_fd(int loop, int fd, enum ml_fd_event rw)
{
  struct kevent ev;
  EV_SET(&ev, fd, rw == ML_READ ? EVFILT_READ : EVFILT_WRITE, EV_DELETE, 0, 0, NULL);
  return kevent(loop, &ev, 1, NULL, 0, NULL);
}

