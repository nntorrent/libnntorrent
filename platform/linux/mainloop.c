#include "../../mainloop.h"

#include <sys/epoll.h>
#include <sys/timerfd.h>
#include <sys/eventfd.h>
#include <errno.h>

int
ml_create()
{
  return epoll_create(1);
}

void
ml_destroy(int loop)
{
  close(loop);
}

/* return positive for number of events, negative for error
 * input ml_event* for event output
 * input size to specify the maximum size of ml_event* and size is modified to specify
 * the actual size of output ml_event*
 */
int
ml_get_events(int loop, ml_event_t *events, int size)
{
  return epoll_wait(loop, events, size, -1);
}

/* returns positive for id, negative for error */
int
ml_create_timer(int loop, int id)
{
  int fd = timerfd_create(CLOCK_MONOTONIC, 0);
  if (fd == -1) {
    return -1;
  }

  struct epoll_event ev;
  /* this is just to quiet valgrind down */
  memset(&ev, 0, sizeof(struct epoll_event));
  ev.data.fd = fd;
  ev.events = EPOLLIN;
  epoll_ctl(loop, EPOLL_CTL_ADD, fd, &ev);

  return fd;
}

int
ml_set_timer(int loop, int id, int timeout, bool oneshot)
{
  struct timespec ts;
  ts.tv_sec = timeout;
  ts.tv_nsec = 0;

  struct itimerspec itspec;
  itspec.it_value = ts;

  if (oneshot) {
    struct timespec ts0;
    ts0.tv_sec = 0;
    ts0.tv_nsec = 0;
    itspec.it_interval = ts0;
  } else {
    itspec.it_interval = ts;
  }

  return timerfd_settime(id, 0, &itspec, NULL);
}

/* return 0 for success, negative for error */
int
ml_destroy_timer(int loop, int id)
{
  close(id);
  return 0;
}

int
ml_create_custom(int loop, int id)
{
  int fd = eventfd(0, 0);

  struct epoll_event ev;
  /* this is just to quiet valgrind down */
  memset(&ev, 0, sizeof(struct epoll_event));
  ev.data.fd = fd;
  ev.events = EPOLLIN | EPOLLRDHUP;

  if (epoll_ctl(loop, EPOLL_CTL_ADD, fd, &ev) != 0) {
    return -1;
  }

  return fd;
}

int
ml_notify_custom(int loop, int id)
{
  uint64_t u = 1;
  if (write(id, &u, sizeof(uint64_t)) != sizeof(uint64_t)) {
    return -1;
  }
  return 0;
}

int
ml_destroy_custom(int loop, int id)
{
  return epoll_ctl(loop, EPOLL_CTL_DEL, id, NULL);
}

bool
ml_event_custom(ml_event_t *ev, int id)
{
  if (ev->data.fd != id) {
    return false;
  }

  uint64_t u;
  if (read(id, &u, sizeof(uint64_t)) <= 0) {
    perror("reading custom event (eventfd)");
    return false;
  }

  return true;
}

bool
ml_event_timer(ml_event_t *ev, int id)
{
  if (ev->data.fd != id)
    return false;

  uint64_t exp_num;
  if (read(id, &exp_num, sizeof(uint64_t)) != sizeof(uint64_t)) {
    perror("ERROR: mainloop.c: tracker timer fd queue is messed up");
  }

  return true;
}

void *
ml_event_data(ml_event_t *ev)
{
  return ev->data.ptr;
}

bool
ml_event_read(ml_event_t *ev)
{
  return ev->events & EPOLLIN;
}

bool
ml_event_write(ml_event_t *ev)
{
  return ev->events & EPOLLOUT;
}

bool
ml_event_eof(ml_event_t *ev)
{
  return ev->events & EPOLLRDHUP;
}

int
ml_event_fd(ml_event_t *ev)
{
  return ev->data.fd;
}

int
ml_add_fd(int loop, int fd, enum ml_fd_event rw, void *data)
{
  struct epoll_event ev;
  /* this is just to quiet valgrind down */
  memset(&ev, 0, sizeof(struct epoll_event));
  if (data) {
    ev.data.ptr = data;
  } else {
    ev.data.fd = fd;
  }
  ev.events = rw == ML_READ ? EPOLLIN | EPOLLRDHUP : EPOLLOUT;

  int ret = 0;
  if ((ret = epoll_ctl(loop, EPOLL_CTL_ADD, fd, &ev)) != 0) {
    if (errno == EEXIST) {
      ret = epoll_ctl(loop, EPOLL_CTL_MOD, fd, &ev);;
    }
  }

  return ret;
}

int
ml_del_fd(int loop, int fd, enum ml_fd_event rw)
{
  return epoll_ctl(loop, EPOLL_CTL_DEL, fd, NULL);
}

