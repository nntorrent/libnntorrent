#include "torrent.h"

#include <time.h>
#include <stdlib.h>
#include <errno.h>
#include <openssl/sha.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <syslog.h>

#include "mainloop.h"
#include "evqueue.h"
#include "constants.h"

/* initialization functions */
int init_single_file(torrent_t *, const char *);
int init_multi_file(torrent_t *, const char *);
int init_file_maps(torrent_t *);
int init_tracker(torrent_t *);
int init_peers(torrent_t *);

torrent_t *nn_torrent_new(const char *filename, const char *destdir) {
  srand(time(NULL));

  torrent_t *to = malloc(sizeof(torrent_t));
  memset(to, 0, sizeof(torrent_t));
  to->next = NULL;
  to->done = false;
  to->do_hash_check = false;
  to->quit = false;
  to->id = NULL;

  to->peers_num = 0;
  to->last_peer_checked = 0;

  pthread_mutex_init(&to->peer_queue_mutex, NULL);

  /*
   * setup file descriptors
   */
  to->loop_fd = ml_create();
  to->tracker_timer_id = ml_create_timer(to->loop_fd, 1);
  if (to->tracker_timer_id == -1) {
    syslog(LOG_DEBUG, "could not create timer: %m");
  }

  to->peer_timer_id = ml_create_timer(to->loop_fd, 2);
  if (to->peer_timer_id == -1) {
    syslog(LOG_DEBUG, "could not create timer: %m");
  }

  if (pipe(to->selfpipe) != 0) {
    syslog(LOG_WARNING, "could not initialize signalling for torrent");
  }

  ml_add_fd(to->loop_fd, to->selfpipe[0], ML_READ, 0);

  /*
   * parse metainfo
   */
  to->metainfo = ti_parse_torrent_file(filename);
  if (!to->metainfo) {
    syslog(LOG_ERR, "could not parse %s", filename);
    return NULL;
  }

  to->id = malloc(TORRENT_ID_SIZE);
  memset(to->id, 0, TORRENT_ID_SIZE);
  char temp[3];
  for (int i = 0; i < INFO_HASH_SIZE; i++) {
    snprintf(temp, 3, "%.2x", to->metainfo->info_hash[i]);
    memcpy(to->id + i*2, temp, 2);
  }

  if (to->metainfo->info->type == TORRENT_FILE_MODE_SINGLE)
    init_single_file(to, destdir);
  else
    init_multi_file(to, destdir);

  nn_torrent_calc_dimensions(to);

  /* assuming piece length of 1MB (~2^20 bytes), we'll need
   * 2^20 / 2^14 = 2^6 = 1 * 64bit integer
   * per piece to address all indexes in a reasonably large piece length
   *
   * conventionally, piece length is mostly 256kB, 512kB, and 1MB, so 64 bits should
   * be enough, but using gmp lib for 256bit integers is more robust to handle *any* piece
   * length.
   */
  to->indexfield = malloc(to->piece_num * sizeof(mpz_t));
  for (int i = 0; i < to->piece_num; i++) {
    mpz_init2(to->indexfield[i], to->indexes_len);
  }

  for (int i = 0; i < PENDING_REQUESTS_QUEUE_MAX; i++) {
    to->pending_requests_queue[i].index = -1;
    to->pending_requests_queue[i].begin = -1;
    to->pending_requests_queue[i].length = -1;
    to->pending_requests_queue[i].peer = NULL;
  }

  /* and initialize the bitfield, which is a different thing than
   * the indexfield. sole purpose of this bitfield is for sending to
   * peers
   */
  to->bitfield = malloc(sizeof(bitfield_t));
  bitfield_init(to->bitfield, to->piece_num);

  evqueue_enqueue(evqueue_event_new(to->id, EVQUEUE_EVENT_ADDED));

  return to;
}

int
nn_torrent_arm_tracker_timer(torrent_t *t, int sec) {
  return ml_set_timer(t->loop_fd, t->tracker_timer_id, sec, true);
}

int
nn_torrent_arm_peer_timer(torrent_t *t, int sec) {
  return ml_set_timer(t->loop_fd, t->peer_timer_id, sec, false);
}

void *
nn_torrent_run(void *input_torrent) {
  torrent_t *t = (torrent_t *) input_torrent;

  if (init_file_maps(t) != 0) {
    nn_torrent_shutdown(t);
    return NULL;
  }

  if (init_tracker(t) != 0) {
    nn_torrent_shutdown(t);
    return NULL;
  }

  if (t->do_hash_check) {
    nn_torrent_check_full_hash(t);
  }

  if (!nn_torrent_is_complete(t)) {
    nn_torrent_discover_peers(t);
    nn_torrent_arm_peer_timer(t, 60);
  }

  do {
    ml_event_t eevents[10];
    int num_events = ml_get_events(t->loop_fd, eevents, 10);

    if (num_events < 0) {
      syslog(LOG_ERR, "torrent '%s' mainloop error occurred: %m", t->metainfo->info->name);
      break;
    }

    for (int i = 0; i < num_events; i++) {
      /*
       * wake up call
       */
      if (ml_event_read(&eevents[i]) && ml_event_fd(&eevents[i]) == t->selfpipe[0]) {
        char buf;
        read(t->selfpipe[0], &buf, 1);
      }

      /*
        * update tracker every interval
        */
      else if (ml_event_timer(&eevents[i], t->tracker_timer_id)) {
        tracker_event_t tev = t->tr->req->event;
        t->tr->req->event = TRACKER_EVENT_NONE;
        nn_torrent_update_tracker(t);
        t->tr->req->event = tev;
      }

      /*
       * checking every about 60 seconds to see if we have enough
       * peers attached to us
       */
      else if (ml_event_timer(&eevents[i], t->peer_timer_id)) {
        if (nn_torrent_is_complete(t)) {
          nn_torrent_arm_peer_timer(t, 0);
        } else {
          nn_torrent_discover_peers(t);
        }

        continue;
      }

      /*
       * peer closed connection
       */
      else if (ml_event_data(&eevents[i]) && ml_event_eof(&eevents[i])) {
        peer_t *p = (peer_t *) ml_event_data(&eevents[i]);
        nn_torrent_shutdown_peer(t, p);
        continue;
      }

      /*
        * get data from peers
        */
      else if (ml_event_data(&eevents[i]) && ml_event_read(&eevents[i])) {
        /* a peer is sending a message */
        peer_t *p = (peer_t *) ml_event_data(&eevents[i]);

        if (!(peer_state(p) & PEER_READY)) {
          if (peer_receive_handshake(p) != 0) {
            nn_torrent_shutdown_peer(t, p);
            continue;
          }
        }

        if (peer_get_message(p) != 0) {
          nn_torrent_shutdown_peer(t, p);
        } else {
          nn_torrent_respond(t, p);
        }

        continue;
      }

      /*
        * get connection from pending peers
        */
      else if (ml_event_data(&eevents[i]) && ml_event_write(&eevents[i])) {
        /* an outgoing peer connection is ready or failed */
        peer_t *p = (peer_t *) ml_event_data(&eevents[i]);
        int sock_error = 0;
        socklen_t sock_error_len = sizeof(sock_error);

        if (getsockopt(p->sock, SOL_SOCKET, SO_ERROR, &sock_error, &sock_error_len) != 0) {
          syslog(LOG_DEBUG, "could not get outgoing peer sock error condition: %m");
          continue;
        }

        if (sock_error == 0) {
          /* make the socket blocking for read/write since mainloop takes care of multiplexing */
          int flags = 0;
          if ((flags = fcntl(p->sock, F_GETFL, 0)) < 0) {
            syslog(LOG_DEBUG, "could not get sock %d flags: %m", p->sock);
          } else {
            flags &= ~(O_NONBLOCK);
            if (fcntl(p->sock, F_SETFL, flags) != 0) {
              syslog(LOG_DEBUG, "could not set sock %d flags: %m", p->sock);
            }
          }

          ml_del_fd(t->loop_fd, p->sock, ML_WRITE);
          ml_add_fd(t->loop_fd, p->sock, ML_READ, p);

          peer_send_handshake(p, t->metainfo->info_hash, t->tr->req->peer_id);
          size_t len;
          unsigned char *bits = bitfield_export(t->bitfield, &len);
          peer_bitfield(p, bits, len);
          free(bits);
        } else {
          struct in_addr m_ip;
          m_ip.s_addr = p->ip;

          syslog(LOG_DEBUG, "cannot connect to peer %s:%d, shutting it down",
                 inet_ntoa(m_ip), p->port);
          ml_del_fd(t->loop_fd, p->sock, ML_WRITE);
          close(p->sock);

          nn_torrent_shutdown_peer(t, p);
        }

        continue;
      }


      else {
        syslog(LOG_ERR, "unknown event detected");
      }
    } /* for */
  } while (!t->quit); /* do-while */

  nn_torrent_shutdown(t);

  return NULL;
}

void nn_torrent_free(torrent_t *t) {
  if (!t)
    return;

  if (t->tr)
    tracker_free(t->tr);

  if (t->id)
    free(t->id);

  if (t->metainfo)
    ti_free(t->metainfo);

  peer_t *p = t->first_peer;
  while (p) {
    peer_t *temp = p;
    p = p->next;

    peer_free(temp);
  }

  if (t->indexfield) {
    for (int i = 0; i < t->piece_num; i++) {
      mpz_clear(t->indexfield[i]);
    }
    free(t->indexfield);
  }

  if (t->bitfield) {
    bitfield_free(t->bitfield);
    free(t->bitfield);
  }

  if (t->destfiles) {
    for (int i = 0; i < t->destfiles_num; i++) {
      munmap(t->destfiles[i].data, t->destfiles[i].length);
      close(t->destfiles[i].fd);
    }

    free(t->destfiles);
  }

  close(t->loop_fd);
  close(t->tracker_timer_id);
  close(t->peer_timer_id);
  close(t->selfpipe[0]);
  close(t->selfpipe[1]);

  free(t);
}

int
nn_torrent_get_message(torrent_t *t, peer_t *p)
{
  return 0;
}

int
nn_torrent_respond(torrent_t *t, peer_t *p)
{
  peer_message_t *m = peer_queue_pop(p);

  // if nothing's happening, try to do something
  if (!m && !nn_torrent_is_complete(t)) {
    if (peer_state(p) & PEER_AM_CHOKED && ! (peer_state(p) & PEER_AM_INTERESTED)) {
      peer_interested(p);
    } else if (! (peer_state(p) & PEER_AM_CHOKED) && p->pending_requests <= 0) {
      nn_torrent_request(t, p);
    }

    return 0;
  }

  while (m) {
    switch (m->type) {
      case PEER_MESSAGE_PIECE:
        nn_torrent_handle_piece(t, p, m);
        break;

      case PEER_MESSAGE_REQUEST:
        {
          unsigned char *data = malloc(m->block_len);
          if (nn_torrent_handle_request(t, p, m, data) == 0) {
            peer_piece(p, m->index, m->begin, data, m->block_len);
          }
          free(data);
        }
        break;

      case PEER_MESSAGE_CHOKE:
      case PEER_MESSAGE_UNCHOKE:
      case PEER_MESSAGE_HAVE:
      case PEER_MESSAGE_NONE:
      case PEER_MESSAGE_BITFIELD:
        // peer has already done what's needed for these messages
        if (!nn_torrent_is_complete(t)) {
          if (peer_state(p) & PEER_AM_CHOKED && !(peer_state(p) & PEER_AM_INTERESTED)) {
            peer_interested(p);
          } else if ((!(peer_state(p) & PEER_AM_CHOKED)) && p->pending_requests <= 0) {
            nn_torrent_request(t, p);
          }
        }
        break;

      case PEER_MESSAGE_INTERESTED:
        peer_unchoke(p);
        break;

      case PEER_MESSAGE_INVALID:
        syslog(LOG_DEBUG, "got invalid message");
      case PEER_MESSAGE_SHUTDOWN:
        nn_torrent_shutdown_peer(t, p);
        return 0;

      default:
        syslog(LOG_WARNING, "unknown message received");
        break;
    }

    peer_message_free(m);
    m = peer_queue_pop(p);
  }

  return 0;
}

int
nn_torrent_shutdown_peer(torrent_t *t, peer_t *p)
{
  for (int i = 0; i < PENDING_REQUESTS_QUEUE_MAX; i++) {
    if (t->pending_requests_queue[i].peer == p) {
      t->pending_requests_queue[i].peer = NULL;
      t->pending_requests_queue[i].index = -1;
      t->pending_requests_queue[i].begin = -1;
      t->pending_requests_queue[i].length = -1;
      t->pending_requests_queue_size--;
    }
  }

  nn_torrent_del_peer(t, p);
  return 0;
}

int
nn_torrent_request(torrent_t *t, peer_t *p)
{
  if (t->pending_requests_queue_size >= PENDING_REQUESTS_QUEUE_MAX) {
    syslog(LOG_DEBUG, "pending requests queue full, cannot request new pieces");
    return 0;
  }

  if (nn_torrent_is_complete(t)) {
    syslog(LOG_DEBUG, "torrent is already complete");
    return 0;
  }

  if (p->pending_requests >= PEER_MAX_PENDING_REQUESTS)
    return 0;

  int piece = 0;
  int index = 0;

  while (p->pending_requests < PEER_MAX_PENDING_REQUESTS) {
    if (piece > t->last_piece ||
        (piece == t->last_piece && index > t->last_index)) {
      break;
    }

    if (index >= t->indexes_len || bitfield_tstbit(t->bitfield, piece)) {
      piece++;
      index = 0;
      continue;
    }

    if (mpz_tstbit(t->indexfield[piece], index)) {
      index++;
      continue;
    }

    peer_request(p,
                 piece,
                 index * t->request_size,
                 (piece == t->last_piece && index == t->last_index) ?
                   t->last_index_size : t->request_size);

    /*
     * add it to pending requests queue
     */
    for (int i = 0; i < PENDING_REQUESTS_QUEUE_MAX; i++) {
      if (t->pending_requests_queue[i].index < 0) {
        t->pending_requests_queue[i].index = piece;
        t->pending_requests_queue[i].begin = index * t->request_size;
        t->pending_requests_queue[i].length =
          (piece == t->last_piece && index == t->last_index) ?  t->last_index_size
                                                             : t->request_size;
        t->pending_requests_queue[i].peer = p;
        t->pending_requests_queue_size++;

        break;
      }
    }

    index++;
  }

  return 0;
}

int
nn_torrent_handle_piece(torrent_t *t, peer_t *p, peer_message_t *m)
{
  if (mpz_tstbit(t->indexfield[m->index], m->begin / t->request_size)) {
    syslog(LOG_DEBUG, "already have piece %d, throwing it away", m->index);
    return 0;
  }

  int max_chunk = m->index == t->last_piece ? t->last_index + 1: t->indexes_len;

  /* saving the data
   *
   * if the torrent only has one file, then just plop the blob right in.
   *
   * if not, however, then we have to figure out which file it belongs to and
   * how much to write to that file if the chunk hits a boundary.
   */
  if (t->metainfo->info->type == TORRENT_FILE_MODE_SINGLE) {
    // plop the blob
    memcpy(t->destfiles[0].data + m->index * t->metainfo->info->piece_length + m->begin, m->block, m->block_len);
  } else {
    /*
     * which file does it belong to?
     *
     * the file whose start is lower than the start of the block.
     * if the block flows over the file, then the rest is plopped into the next block.
     *
     * here, the start of the block is
     *    piece index * pieces size + begin offset
     *
     * and thus the offset of the file would be
     *    start of the block - start of the file
     */
    size_t block_start = m->index * t->metainfo->info->piece_length + m->begin;

    int f;
    for (f = t->destfiles_num - 1; f >= 0; f--) {
      if (t->destfiles[f].start <= block_start) {
        /* found the first file that this block
         * fits in
         */
        break;
      }
    }

    if (f >= t->destfiles_num || f < 0) {
      syslog(LOG_WARNING, "piece %d does not belong to any file", m->index);
      return -1;
    }

    /*
     * An example of how these numbers are calculated
     *
     *      file 1               file 2              file 3
     * |---------------------|------------|-----------------------------|
     * ^                     |            |          file 1 start
     *                       ^            |          file 1 end, file 2 start
     *                                    ^          file 2 end, file 3 start
     *     ^                                         m->block
     *     ^---------------------------------------^ m->block_len
     *     ^                                         block_start
     *     ^---------------------^                   already_copied = m->block_len - block_leftover
     *                           ^-----------------^ block_leftover
     *                       ^---^                   file_offset
     *                           ^--------^          max_len
     *                           ^--------^          copy_len
     *                           ^                   m->block + already_copied
     */

    size_t block_leftover = m->block_len;
    while (block_leftover) {
      /* the data offset within this file where copying starts */
      size_t already_copied = m->block_len - block_leftover;
      size_t file_offset = block_start + already_copied - t->destfiles[f].start;

      /* maximum block length that this file can accept */
      size_t max_len = t->destfiles[f].length - file_offset;

      /*
       * the actual length of the data that should be copied to file
       * starting at the file's offset
       */
      size_t copy_len = block_leftover > max_len ? max_len : block_leftover;

      memcpy(t->destfiles[f].data + file_offset, m->block + already_copied, copy_len);

      block_leftover -= copy_len;
      f++;
    }
  }

  t->tr->req->downloaded += m->block_len;

  mpz_setbit(t->indexfield[m->index], m->begin / t->request_size);

  /*
   * cancel other similar pending requests
   */
  for (int i = 0; i < PENDING_REQUESTS_QUEUE_MAX; i++) {
    if (t->pending_requests_queue[i].index == m->index &&
        t->pending_requests_queue[i].begin == m->begin &&
        t->pending_requests_queue[i].length == m->block_len) {

      /*
       * send cancel message only to *other* peers
       */
      if (t->pending_requests_queue[i].peer != p) {
        peer_cancel(t->pending_requests_queue[i].peer,
                    t->pending_requests_queue[i].index,
                    t->pending_requests_queue[i].begin,
                    t->pending_requests_queue[i].length);
      }

      t->pending_requests_queue[i].peer = NULL;
      t->pending_requests_queue[i].index = -1;
      t->pending_requests_queue[i].begin = -1;
      t->pending_requests_queue[i].length = -1;

      t->pending_requests_queue_size--;
    }
  }

  // is a full piece done?
  bool done = true;
  for (int i = 0; i < max_chunk; i++) {
    if (!mpz_tstbit(t->indexfield[m->index], i)) {
      done = false;
      break;
    }
  }

  if (done) {
    if (nn_torrent_check_piece_integrity(t, m->index) == 0) {
      bitfield_setbit(t->bitfield, m->index);
      nn_torrent_announce_have(t, m->index);
    } else {
      // piece isn't *integrated*, scrap it
      syslog(LOG_WARNING, "piece %d integrity check failed", m->index);
      for (int i = 0; i < max_chunk; i++) {
        mpz_clrbit(t->indexfield[m->index], i);
      }

      done = false;
    }

  }

  if (nn_torrent_check_completion(t)) {
    syslog(LOG_INFO, "Torrent is complete: '%s'", t->metainfo->info->name);
  } else {
    peer_message_t *m = peer_message_new(PEER_MESSAGE_NONE);
    peer_queue_push(p, m);
    peer_message_free(m);
  }

  return 0;
}

int
nn_torrent_check_piece_integrity(torrent_t *t, int index)
{
  size_t block_start = index * t->metainfo->info->piece_length;
  int f;
  for (f = t->destfiles_num - 1; f >= 0; f--) {
    if (t->destfiles[f].start <= block_start) {
      // this block fits in this file
      break;
    }
  }

  if (f >= t->destfiles_num || f < 0) {
    syslog(LOG_WARNING, "piece %d does not belong to any file", index);
    return -1;
  }

  size_t piece_length = index == t->last_piece ? t->last_piece_size : t->metainfo->info->piece_length;

  unsigned char *piece_hash = malloc(SHA_DIGEST_LENGTH);
  unsigned char *dn_hash = malloc(SHA_DIGEST_LENGTH);
  unsigned char *data = malloc(piece_length);

  /* see nn_torrent_handle_piece for explanation */
  size_t block_leftover = piece_length;
  while (block_leftover) {
    size_t already_copied = piece_length - block_leftover;
    size_t file_offset = block_start + already_copied - t->destfiles[f].start;
    size_t max_len = t->destfiles[f].length - file_offset;
    size_t copy_len = block_leftover > max_len ? max_len : block_leftover;

    memcpy(data + already_copied, t->destfiles[f].data + file_offset, copy_len);

    block_leftover -= copy_len;
    f++;
  }

  memcpy(piece_hash, t->metainfo->info->pieces + (index * SHA_DIGEST_LENGTH), SHA_DIGEST_LENGTH);
  SHA1(data, piece_length, dn_hash);

  int ret = memcmp(piece_hash, dn_hash, SHA_DIGEST_LENGTH);

  if (ret == 0) {
    t->tr->req->left -= piece_length;
  }

  free(dn_hash);
  free(piece_hash);
  free(data);
  return ret;
}

int
nn_torrent_check_full_hash(torrent_t *t)
{
  syslog(LOG_DEBUG, "checking hash");

  for (unsigned int i = 0; i < t->piece_num; i++) {
    if (nn_torrent_check_piece_integrity(t, i) == 0) {
      bitfield_setbit(t->bitfield, i);
    }
  }

  nn_torrent_check_completion(t);
  t->do_hash_check = false;
  return 0;
}

bool
nn_torrent_check_completion(torrent_t *t)
{
  t->done = true;
  for (int i = 0; i <= t->last_piece; i++) {
    if (!bitfield_tstbit(t->bitfield, i)) {
      t->done = false;
      return false;
    }
  }

  t->tr->req->numwant = 0;
  t->tr->req->event = TRACKER_EVENT_COMPLETED;
  syslog(LOG_DEBUG, "torrent is complete, announcing completion to tracker");
  nn_torrent_update_tracker(t);
  nn_torrent_arm_peer_timer(t, 0);

  for (peer_t *p = t->first_peer; p; p = p->next) {
    peer_notinterested(p);
  }

  /* don't send an event if it was already downloaded */
  if (t->tr->req->downloaded > 0) {
    evqueue_enqueue(evqueue_event_new(t->id, EVQUEUE_EVENT_COMPLETED));
  }

  return true;
}

bool
nn_torrent_is_complete(torrent_t *t)
{
  return t->done;
}

int
nn_torrent_handle_request(torrent_t *t, peer_t *p, peer_message_t *m, unsigned char *data)
{
  if (!bitfield_tstbit(t->bitfield, m->index)) {
    syslog(LOG_DEBUG, "piece %d is not available, ignoring request", m->index);
    return -1;
  }

  if (t->metainfo->info->type == TORRENT_FILE_MODE_SINGLE) {
    // give that man what he wants!
    memcpy(data, t->destfiles[0].data + m->index * t->metainfo->info->piece_length + m->begin, m->block_len);
  } else {
    /*
     * which file does it belong to?
     */
    int block_start = m->index * t->metainfo->info->piece_length + m->begin;

    int f;
    for (f = t->destfiles_num - 1; f >= 0; f--) {
      if (t->destfiles[f].start <= block_start) {
        // this block fits in this file
        break;
      }
    }

    if (f >= t->destfiles_num || f < 0) {
      syslog(LOG_WARNING, "piece %d does not belong to any file", m->index);
      return -1;
    }

    size_t block_leftover = m->block_len;
    while (block_leftover) {
      /* the data offset within this file where copying starts */
      size_t already_copied = m->block_len - block_leftover;
      size_t file_offset = block_start + already_copied - t->destfiles[f].start;

      /* maximum additional block length that this file can accept */
      size_t max_len = t->destfiles[f].length - file_offset;

      /*
       * the actual length of the data that should be copied to buffer
       * starting at the file's offset
       */
      size_t copy_len = block_leftover > max_len ? max_len : block_leftover;

      memcpy(data + already_copied, t->destfiles[f].data + file_offset, copy_len);

      block_leftover -= copy_len;
      f++;
    }
  }

  t->tr->req->uploaded += m->block_len;

  return 0;
}

void
nn_torrent_calc_dimensions(torrent_t *t)
{
  t->total_length = 0;
  for (int i = 0; i < t->destfiles_num; i++)
    t->total_length += t->destfiles[i].length;

  size_t length = t->total_length;
  size_t piece_length = t->metainfo->info->piece_length;
  t->piece_num = ceil((float) length / (float) piece_length);

  /* >>> for i in range(1, 25):
   * ...   for j in range(1, min(14+1, i+1)):
   * ...     print("%d %d %2f" % (i, j, 2.0**i / 2.0**j))
   *
   * NOTE: piece length and request size are always a power of 2
   *
   * where i := piece length and j := index size := request size
   *
   * the above code sample from python shows that if we take request size
   * as the minimum of either 14 or piece_length, we should be able to get
   *
   * why 14? because almost all clients expect *at most* 2^14 byte requests.
   * see BEP 0003 for more information.
   *
   */
  t->request_size = piece_length > MAX_REQUEST_SIZE ? MAX_REQUEST_SIZE : piece_length;
  t->index_num = ceil((float) t->total_length / t->request_size);
  t->indexes_len = piece_length / t->request_size;

  /* last piece size is irregular, so we have to adjust for that, otherwise we
   * may or may not be able to get the last bits of data
   */
  t->last_piece = t->piece_num - 1;
  t->last_piece_size = length - (t->piece_num - 1) * piece_length;

  /* ceil() - 1 instead of floor() because sometimes, it may be the case
   * that the last index is, in fact, the exact size of a full request
   */
  t->last_index = ceil((float) t->last_piece_size / t->request_size) - 1;
  t->last_index_size = t->last_piece_size - t->last_index * t->request_size;

  t->left_requests = t->index_num;
}

int
nn_torrent_del_peer(torrent_t *t, peer_t *p)
{
  close(p->sock);

  pthread_mutex_lock(&t->peer_queue_mutex);
  for (peer_t *p2 = t->first_peer; p2; p2 = p2->next) {
    if (p2 == p) {
      if (p2 == t->first_peer) {
        t->first_peer = p2->next;
        peer_free(p2);
      } else {
        if (p2->prev)
          p2->prev->next = p2->next;

        if (p2->next)
          p2->next->prev = p2->prev;

        peer_free(p2);
      }

      t->peers_num--;
      break;
    }
  }


  pthread_mutex_unlock(&t->peer_queue_mutex);
  /* didn't find it */
  return 0;
}

/* this function is thread-safe */
int
nn_torrent_add_incoming_peer(torrent_t *t, peer_t *p)
{
  ml_add_fd(t->loop_fd, p->sock, ML_READ, p);

  pthread_mutex_lock(&t->peer_queue_mutex);
  int ret = nn_torrent_add_peer(t, p);
  pthread_mutex_unlock(&t->peer_queue_mutex);

  if (peer_send_handshake(p, t->metainfo->info_hash, t->tr->req->peer_id) != 0) {
    nn_torrent_shutdown_peer(t, p);
    return -1;
  }

  size_t len;
  unsigned char *bits = bitfield_export(t->bitfield, &len);
  peer_bitfield(p, bits, len);
  free(bits);

  return ret;
}

int
nn_torrent_add_peer(torrent_t *t, peer_t *p)
{
  if (!t->first_peer) {
    t->first_peer = p;
  } else {
    peer_t *p2;
    for (p2 = t->first_peer; p2->next; p2 = p2->next);

    p2->next = p;
    p->prev = p2;
  }

  t->peers_num++;

  return 0;
}

int
init_single_file(torrent_t *to, const char *destdir)
{
  to->destfiles_num = 1;
  to->destfiles = malloc(sizeof(file_t));

  file_t f;
  f.start = 0;
  f.length = to->metainfo->info->length;
  f.end = f.start + f.length;

  int path_len = strlen(destdir) + 1 + strlen(to->metainfo->info->name) + 1;
  char path[path_len];
  memset(path, 0, path_len);
  strncat(path, destdir, strlen(destdir));
  strcat(path, "/");
  strncat(path, to->metainfo->info->name, path_len - strlen(destdir) - 1);

  if ((f.fd = open(path, O_RDWR | O_CREAT | O_EXCL, (mode_t) 0644)) == -1) {
    if (errno == EEXIST) {
      f.fd = open(path, O_RDWR | O_CREAT, (mode_t) 0644);
      to->do_hash_check = true;
    } else {
      syslog(LOG_DEBUG, "could not open '%s': %m", path);
    }
  }

  f.data = NULL;
  memcpy(to->destfiles, &f, sizeof(file_t));

  to->total_length = f.length;

  return 0;
}

int
init_multi_file(torrent_t *to, const char *destdir)
{
  to->destfiles_num = to->metainfo->info->files_length;
  to->destfiles = malloc(to->destfiles_num * sizeof(file_t));

  int dir_path_len = strlen(destdir)
               + 1
               + strlen(to->metainfo->info->name)
               + 1;
  char dir_path[dir_path_len];
  memset(dir_path, 0, dir_path_len);
  strcat(dir_path, destdir);
  if (destdir[strlen(destdir) - 1] != '/') {
    strcat(dir_path, "/");
  }
  strcat(dir_path, to->metainfo->info->name);

  syslog(LOG_DEBUG, "creating directory %s", dir_path);
  if (mkdir(dir_path, 0777) != 0) {
    if (errno != EEXIST) {
      syslog(LOG_ERR, "could not create directory %s: %m", dir_path);
      return -1;
    }
    syslog(LOG_INFO, "%s already exists", dir_path);
  }

  size_t cur_start = 0;
  for (int i = 0; i < to->destfiles_num; i++) {
    file_t f;
    f.start = cur_start;
    f.length = to->metainfo->info->files[i]->length;
    f.end = f.start + f.length;

    int path_len = dir_path_len
                 + 1
                 + strlen(to->metainfo->info->files[i]->path[to->metainfo->info->files[i]->path_length -1])
                 + 1;
    char path[path_len];
    memset(path, 0, path_len);
    strncpy(path, dir_path, dir_path_len);
    strcat(path, "/");
    strcat(path, to->metainfo->info->files[i]->path[to->metainfo->info->files[i]->path_length -1]);

    syslog(LOG_DEBUG, "opening file %s", path);
    if ((f.fd = open(path, O_RDWR | O_CREAT | O_EXCL, (mode_t) 0644)) == -1) {
      if ((f.fd = open(path, O_RDWR | O_CREAT, (mode_t) 0644)) == -1) {
        syslog(LOG_DEBUG, "could not open '%s': %m", path);
      } else {
        to->do_hash_check = true;
      }
    }

    f.data = NULL;
    memcpy(&to->destfiles[i], &f, sizeof(file_t));

    cur_start += f.length;
  }

  return 0;
}

int
init_file_maps(torrent_t *to)
{
  for (int i = 0; i < to->destfiles_num; i++) {
    if (lseek(to->destfiles[i].fd, to->destfiles[i].length, SEEK_SET) == -1) {
      syslog(LOG_DEBUG, "could not lseek torrent '%s' file %d: %m",
             to->metainfo->info->name, i);
    }

    if (write(to->destfiles[i].fd, "", 1) <= 0) {
      syslog(LOG_DEBUG, "writing to torrent '%s' file %d failed: %m",
             to->metainfo->info->name, i);
    }

    to->destfiles[i].data = mmap(NULL, to->destfiles[i].length, PROT_READ | PROT_WRITE, MAP_SHARED, to->destfiles[i].fd, 0);
    if (to->destfiles[i].data == MAP_FAILED) {
      syslog(LOG_ERR, "could not initialize file maps: %m");
      return -1;
    }
  }

  return 0;
}

int
init_tracker(torrent_t *to)
{
  to->tr = tracker_new();
  if (!to->tr) {
    syslog(LOG_ERR, "could not generate tracker from metainfo file");
    return -1;
  }

  tracker_init(to->tr, to->metainfo, 9000);

  to->tr->req->uploaded = 0;
  to->tr->req->downloaded = 0;
  to->tr->req->left = to->total_length;
  to->tr->req->event = TRACKER_EVENT_STARTED;
  to->tr->req->numwant = 200;
  to->tr->req->key = 0x28374655;

  int list_size = to->metainfo->announce_list_size + 1;
  ti_announce_t *temp[list_size];

  temp[0] = to->metainfo->announce;

  for (int i = 1; i < list_size; i++) {
    temp[i] = to->metainfo->announce_list[i-1];
  }

  bool success = false;
  for (int i = 0; i < list_size; i++) {
    ti_announce_t *ann = temp[i];

    syslog(LOG_DEBUG, "Trying tracker '%s'", ann->hostname);
    tracker_set_host(to->tr, ann->hostname, ann->port, ann->protocol);
    tracker_set_url(to->tr, ann->url);

    if (tracker_connect(to->tr) != 0) {
      syslog(LOG_DEBUG, "could not connect to tracker '%s'", ann->hostname);
      continue;
    }

    if (tracker_announce(to->tr) == 0) {
      success = true;
      break;
    }

  }

  if (!success) {
    syslog(LOG_ERR, "could not initialize tracker");
    return -1;
  }

  nn_torrent_arm_tracker_timer(to, to->tr->last_response->interval);
  to->last_peer_checked = 0;
  return 0;
}

int
nn_torrent_discover_peers(torrent_t *t)
{
  if (t->peers_num >= 30) {
    return 0;
  }

  syslog(LOG_DEBUG, "%s: have %d peers, discovering more", t->metainfo->info->name, t->peers_num);
  pthread_mutex_lock(&t->peer_queue_mutex);

  /* how many peers do we have? */
  for(; t->last_peer_checked < t->tr->last_response->peers_len; t->last_peer_checked++) {
    int i = t->last_peer_checked;

    if (t->peers_num >= 30) {
      break;
    }

    bool met = false;
    for (peer_t *ptemp = t->first_peer; ptemp; ptemp = ptemp->next) {
      if (ptemp->ip == t->tr->last_response->peers[i].ipa &&
          ptemp->port == t->tr->last_response->peers[i].port) {
        met = true;
        break;
      }
    }

    if (met) {
      continue;
    }

    struct in_addr m_ip;
    m_ip.s_addr = t->tr->last_response->peers[i].ipa;

    peer_t *p = peer_new_outgoing(t->tr->last_response->peers[i].ipa,
                                  t->tr->last_response->peers[i].port,
                                  t->metainfo->info_hash,
                                  t->tr->req->peer_id);
    if (!p) {
      syslog(LOG_DEBUG, "failed to create new peer %s:%d",
             inet_ntoa(m_ip),
             t->tr->last_response->peers[i].port);

      continue;
    }

    if (peer_connect(p) != 0) {
      peer_free(p);
      continue;
    }

    syslog(LOG_DEBUG, "adding %s:%d to watchlist",
           inet_ntoa(m_ip),
           t->tr->last_response->peers[i].port);

    ml_add_fd(t->loop_fd, p->sock, ML_WRITE, p);
    nn_torrent_add_peer(t, p);
  }

  pthread_mutex_unlock(&t->peer_queue_mutex);

  return 0;
}

int
nn_torrent_update(torrent_t *t)
{
  if (nn_torrent_update_tracker(t) != 0) {
    syslog(LOG_WARNING, "could not update tracker");
    return -1;
  }

  if (nn_torrent_discover_peers(t) != 0) {
    syslog(LOG_WARNING, "could not discover new peers");
  }

  return 0;
}

int
nn_torrent_update_tracker(torrent_t *t)
{
  if (tracker_announce(t->tr) != 0) {
    syslog(LOG_DEBUG, "could not get tracker's announce response");
  }

  /* tracker announce failed, try it again later */
  nn_torrent_arm_tracker_timer(t, t->tr->last_response->interval);
  t->last_peer_checked = 0;

  return 0;
}

void
nn_torrent_shutdown(torrent_t *t)
{
  syslog(LOG_INFO, "shutting down torrent `%s`", t->metainfo->info->name);
  t->tr->req->event = TRACKER_EVENT_STOPPED;

  if (tracker_announce(t->tr) != 0) {
    syslog(LOG_WARNING, "could not announce shutdown event");
  }

  peer_t *p = t->first_peer;
  while (p) {
    peer_t *temp = p->next;
    peer_free(p);
    p = temp;
  }

  t->first_peer = NULL;
}

void
nn_torrent_quit(torrent_t *t)
{
  fprintf(stderr, "quitting torrent\n");
  t->quit = true;
  nn_torrent_wakeup(t);
}

void
nn_torrent_wakeup(torrent_t *t)
{
  write(t->selfpipe[1], "x", 1);
}

int
nn_torrent_announce_have(torrent_t *t, int piece)
{
  for (peer_t *p = t->first_peer; p; p = p->next) {
    if (peer_state(p) & PEER_READY && peer_have(p, piece) != 0) {
      struct in_addr m_ip;
      m_ip.s_addr = p->ip;
      syslog(LOG_DEBUG, "could not send have message for piece %d to peer %s:%d", piece, inet_ntoa(m_ip), p->port);
      /*nn_torrent_shutdown_peer(t, p);*/
    }
  }

  return 0;
}

