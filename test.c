#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <signal.h>
#include <time.h>

#if defined(__FreeBSD__)
#include <sys/endian.h>
#else
#include <endian.h>
#endif

#include <openssl/sha.h>

#include "metainfo.h"
#include "tracker.h"
#include "peer.h"
#include "util.h"

#include <netdb.h>

static bool quit = false;

unsigned char *get_message(int sock, uint32_t *len);

void signal_handler(int sig) {
  fprintf(stderr, "quitting...\n");
  quit = true;
}

int
main(int argc, char **argv)
{
  signal(SIGINT, signal_handler);

  /* PARSING TORRENT INFO */
  torrent_info_t *ti = ti_parse_torrent_file("./test2.torrent");
  if (ti == NULL) {
    fprintf(stderr, "ERROR: main: could not parse torrent file\n");
    return EXIT_FAILURE;
  }

  if (ti->announce != NULL)
    fprintf(stderr, "announce: %s\n", ti->announce->hostname);

  if (ti->announce_list != NULL) {
    fprintf(stderr, "announce-list:\n");
    for (size_t i = 0; i < ti->announce_list_size; i++)
      fprintf(stderr, "\t%s:%d\n", ti->announce_list[i]->hostname, ti->announce_list[i]->port);
  }

  fprintf(stderr, "creation date: %d\n", ti->creation_date);

  if (ti->comment != NULL)
    fprintf(stderr, "comment: %s\n", ti->comment);

  if (ti->created_by != NULL)
    fprintf(stderr, "created by: %s\n", ti->created_by);

  if (ti->encoding != NULL)
    fprintf(stderr, "encoding: %s\n", ti->encoding);

  if (ti->info != NULL) {
    fprintf(stderr, "info name: %s\n", ti->info->name);
    fprintf(stderr, "info piece length: %ld\n", ti->info->piece_length);
    fprintf(stderr, "info private: %d\n", ti->info->is_private);

    if (ti->info->type == TORRENT_FILE_MODE_MULTI) {
      fprintf(stderr, "info multi file\n");
      for (size_t i = 0; i < ti->info->files_length; i++) {
        fprintf(stderr, "\tlength: %ld\n", ti->info->files[i]->length);
        fprintf(stderr, "\tpath:\n");
        for (size_t j = 0; j < ti->info->files[i]->path_length; j++)
          fprintf(stderr, "\t\t%s\n", ti->info->files[i]->path[j]);
      }
    } else {
      fprintf(stderr, "info single file\n");
      fprintf(stderr, "info length: %ld\n", ti->info->length);
      fprintf(stderr, "info md5sum: %s\n", ti->info->md5sum);
    }
  }

  /* BUILDING TRACKER REQUEST URL */
  tracker_t *tr = tracker_new();
  tr->req->event = TRACKER_EVENT_STARTED;
  tr->req->numwant = 30;

  hex_print("info_hash", tr->req->info_hash, SHA_DIGEST_LENGTH);

  if (tracker_connect(tr) != 0) {
    return EXIT_FAILURE;
  }

  fprintf(stderr, "connection_id: %lx\n", tr->req->connection_id);

  tracker_announce(tr);

  /* send handshake */
  peer_t *p = peer_new_outgoing(tr->last_response->peers[0].ipa, tr->last_response->peers[0].port, tr->req->info_hash, tr->req->peer_id);

  if (!p) {
    return EXIT_FAILURE;
  }

  if (! (peer_state(p) & PEER_READY)) {
    fprintf(stderr, "peer is not ready!\n");
    return EXIT_FAILURE;
  }

  /* send bitfield: optional */
  int piece_num = (int) ceil((float) ti->info->pieces_length / 20);
  int bitfield_len = ceil((float) piece_num / 8);
  uint8_t bitfield[bitfield_len];
  memset(bitfield, 0, bitfield_len);
  peer_bitfield(p, bitfield, bitfield_len);

  /* read *all* messages */
  while (1) {
    unsigned char *buf = malloc(2);
    fprintf(stderr, "getting moar messages...\n");
    int ret = recv(p->sock, buf, 2, MSG_PEEK | MSG_DONTWAIT);
    free(buf);

    if (ret > 0) {
      uint32_t len = 0;
      buf = get_message(p->sock, &len);
      peer_handle_message(p, buf, len);
      free(buf);
    } else {
      break;
    }
  }

  fprintf(stderr, "got all messages\n");

  /* we want to download, so we should make ourselves interested */
  peer_interested(p);

  uint32_t len;
  unsigned char *buf = get_message(p->sock, &len);
  peer_handle_message(p, buf, len);
  free(buf);

  peer_request(p, 0, 0, pow(2, 14));

  len = 0;
  buf = get_message(p->sock, &len);
  if (peer_handle_message(p, buf, len) == PEER_MESSAGE_PIECE) {
    fprintf(stderr, "It's a piece message, should be processed\n");
  }
  free(buf);

  peer_free(p);
  ti_free(ti);
  tracker_free(tr);

  return EXIT_SUCCESS;
}

unsigned char *
get_message(int sock, uint32_t *len)
{
  ssize_t l = recv(sock, len, sizeof(uint32_t), 0);

  if (l < 0) {
    perror("recv failed");
    return NULL;
  }

  if (l == 0) {
    fprintf(stderr, "keepalive msg\n");
    return NULL;
  }

  *len = be32toh(*len);

  unsigned char *buf = malloc(*len);
  l = recv(sock, buf, *len, MSG_WAITALL);

  if (l != *len) {
    fprintf(stderr, "could not get all of message\n");
    free(buf);
    return NULL;
  }

  return buf;

}
