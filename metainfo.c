#include "metainfo.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <inttypes.h>

#include <openssl/sha.h>
#include <bencode.h>

torrent_info_t *
ti_new()
{
  torrent_info_t *ret = malloc(sizeof(torrent_info_t));
  memset(ret, 0, sizeof(torrent_info_t));
  ret->info = NULL;
  ret->info_hash = NULL;
  ret->announce = NULL;
  ret->announce_list = NULL;
  ret->announce_list_size = 0;
  ret->creation_date = 0;
  ret->comment = NULL;
  ret->created_by = NULL;
  ret->encoding = NULL;

  return ret;

}
torrent_info_t *
ti_parse_torrent_file(const char *path)
{
  FILE *f = fopen(path, "rb");

  if (!f) {
    syslog(LOG_ERR, "could not open torrent file '%s': %m", path);
    return NULL;
  }

  fseek(f, 0, SEEK_END);
  long len = ftell(f);
  rewind(f);

  unsigned char *content = malloc(len);
  memset(content, 0, len);
  size_t bytes_read = fread(content, 1, len, f);
  if (bytes_read != len) {
    syslog(LOG_ERR, "could not read torrent file '%s': %m", path);
    fclose(f);
    free(content);
    return NULL;
  }
  fclose(f);

  torrent_info_t *ti = ti_parse_torrent_content(content, len);
  free(content);
  return ti;
}

torrent_info_t *
ti_parse_torrent_content(unsigned char *content, size_t size)
{
  bencode_t *ben = bencode_new();
  if (bencode_decode((char *) content, size, ben) != 0) {
    syslog(LOG_ERR, "could not decode torrent file contents");
    bencode_free(ben);
    return NULL;
  }

  torrent_info_t *ti = ti_new();

  ti_get_info_hash(ti, ben);
  ti_get_announce(ti, ben);
  ti_get_announce_list(ti, ben);
  ti_get_creation_date(ti, ben);
  ti_get_comment(ti, ben);
  ti_get_created_by(ti, ben);
  ti_get_encoding(ti, ben);
  ti_get_info(ti, ben);

  bencode_free(ben);

  return ti;
}

torrent_info_t *
ti_parse_magnet_uri(char *uri)
{
  return NULL;
}

char *
ti_get_string_value(bencode_t *ben, char *key)
{
  bencode_t *temp = bencode_new();
  if (bencode_dict_get(ben, key, temp) != 0) {
    syslog(LOG_DEBUG, "could not get key %s from torrent info", key);
    bencode_free(temp);
    return NULL;
  }

  char *res;

  if (temp->type == BENCODE_STR) {
    size_t len;
    char *r = bencode_str_get_value(temp, &len);

    res = malloc(len+1);
    memcpy(res, r, len);
    res[len] = '\0';
    free(r);
  } else {
    res = NULL;
  }

  bencode_free(temp);
  return res;
}

long long
ti_get_int_value(bencode_t *ben, char *key)
{
  bencode_t *temp = bencode_new();
  if (bencode_dict_get(ben, key, temp) != 0) {
    syslog(LOG_DEBUG, "could not get key %s from torrent info", key);
    bencode_free(temp);
    return 0;
  }

  if (temp->type != BENCODE_INT) {
    bencode_free(temp);
    return 0;
  }

  long long ret = bencode_int_get_value(temp);
  bencode_free(temp);
  return ret;
}

void
ti_get_info(torrent_info_t *ti, bencode_t *ben)
{
  bencode_t *temp = bencode_new();
  if (bencode_dict_get(ben, "info", temp) != 0) {
    syslog(LOG_DEBUG, "could not get 'info' key from torrent");
    bencode_free(temp);
    return;
  }

  if (temp->type != BENCODE_DICT) {
    syslog(LOG_DEBUG, "'info' key is not a bencoded dictionary");
    bencode_free(temp);
    return;
  }

  // get info data
  ti_info_t *info = malloc(sizeof(ti_info_t));
  memset(info, 0, sizeof(ti_info_t));

  // check for mode. if it has a length field, it's single file
  // if not, then it's multi file
  bencode_t *length_val = bencode_new();
  if (bencode_dict_get(temp, "length", length_val) == 0) {
    info->type = TORRENT_FILE_MODE_SINGLE;
  } else {
    info->type = TORRENT_FILE_MODE_MULTI;
  }
  bencode_free(length_val);

  // common fields for both single and multi file modes
  info->piece_length = (size_t) ti_get_int_value(temp, "piece length");
  info->is_private = (int) ti_get_int_value(temp, "private");
  info->name = ti_get_string_value(temp, "name");

  bencode_t *b_pieces = bencode_new();
  if (bencode_dict_get(temp, "pieces", b_pieces) != 0) {
    syslog(LOG_DEBUG, "could not get 'pieces' key from torrent");
    bencode_free(b_pieces);
    bencode_free(temp);
    free(info);
    return;
  }

  if (b_pieces->type != BENCODE_STR) {
    syslog(LOG_DEBUG, "'pieces' key is not a bencoded string");
    bencode_free(b_pieces);
    bencode_free(temp);
    free(info);
    return;
  }

  info->pieces = bencode_str_get_value(b_pieces, &info->pieces_length);
  bencode_free(b_pieces);

  if (info->type == TORRENT_FILE_MODE_SINGLE) {
    ti_get_info_single_file(info, temp);
  } else {
    ti_get_info_multi_file(info, temp);
  }

  ti->info = info;
  bencode_free(temp);
}

void
ti_get_info_hash(torrent_info_t *ti, bencode_t *ben)
{
  bencode_t *temp = bencode_new();
  if (bencode_dict_get(ben, "info", temp) != 0) {
    syslog(LOG_DEBUG, "could not get 'info' key from torrent");
    bencode_free(temp);
    return;
  }

  if (temp->type != BENCODE_DICT) {
    syslog(LOG_DEBUG, "'info' key is not a bencoded dictionary");
    bencode_free(temp);
    return;
  }

  // get the SHA-1 hash
  ti->info_hash = malloc(SHA_DIGEST_LENGTH);
  SHA1((unsigned char *) temp->data, temp->data_length, ti->info_hash);
  bencode_free(temp);
}

void
ti_get_info_single_file(ti_info_t *info, bencode_t *ben)
{
  info->length = (size_t) ti_get_int_value(ben, "length");
  info->md5sum = ti_get_string_value(ben, "md5sum");
}

void
ti_get_info_multi_file(ti_info_t *info, bencode_t *ben)
{
  // files is a list of dictionaries
  bencode_t *b_file_list = bencode_new();
  if (bencode_dict_get(ben, "files", b_file_list) != 0) {
    syslog(LOG_DEBUG, "could not get 'files' key from torrent");
    bencode_free(b_file_list);
    return;
  }

  if (b_file_list->type != BENCODE_LIST) {
    syslog(LOG_DEBUG, "'files' key is not a bencoded list");
    bencode_free(b_file_list);
    return;
  }

  info->files_length = b_file_list->length;
  info->files = malloc(info->files_length * sizeof(ti_info_file_t *));
  memset(info->files, 0, info->files_length * sizeof(ti_info_file_t *));

  for (size_t i = 0; i < b_file_list->length; i++) {

    ti_info_file_t *infofile = malloc(sizeof(ti_info_file_t));
    memset(infofile, 0, sizeof(ti_info_file_t));

    bencode_t *b_file_dict = bencode_new();
    if (bencode_list_get(b_file_list, i, b_file_dict) != 0) {
      syslog(LOG_DEBUG, "could not get item %ld in 'files'", i);
      bencode_free(b_file_dict);
      break;
    }

    if (b_file_dict->type != BENCODE_DICT) {
      syslog(LOG_DEBUG, "item %ld in 'files' is not a bencoded dictionary", i);
      bencode_free(b_file_dict);
      break;
    }

    bencode_t *t = bencode_new();

    /*
     * length
     */
    if (bencode_dict_get(b_file_dict, "length", t) == 0 &&
        t->type == BENCODE_INT) {
      infofile->length = (size_t) bencode_int_get_value(t);
    } else {
      syslog(LOG_DEBUG, "failed to decode key 'length' in 'files' at index %ld", i);
    }

    bencode_clear(t);

    /*
     * md5sum
     */
    if (bencode_dict_get(b_file_dict, "md5sum", t) == 0 &&
        t->type == BENCODE_STR) {
      infofile->md5sum = malloc(t->length + 1);
      memcpy(infofile->md5sum, t->value.s, t->length);
      infofile->md5sum[t->length] = '\0';
    } else {
      syslog(LOG_DEBUG, "failed to decode key 'md5sum' in 'files' at index %ld", i);
    }

    bencode_clear(t);

    /*
     * path :: list of strings
     */
    if (bencode_dict_get(b_file_dict, "path", t) == 0 &&
        t->type == BENCODE_LIST) {
      infofile->path = malloc(t->length * sizeof(char *));
      infofile->path_length = t->length;
      memset(infofile->path, 0, t->length * sizeof(char *));

      for (size_t j = 0; j < t->length; j++) {
        bencode_t *p = bencode_new();
        if (bencode_list_get(t, j, p) == 0 &&
            p->type == BENCODE_STR) {
          infofile->path[j] = malloc(p->length + 1);
          memcpy(infofile->path[j], p->value.s, p->length);
          infofile->path[j][p->length] = '\0';
        } else {
          syslog(LOG_DEBUG, "failed to decode index %ld of key 'path' in index %ld of 'files'",
                 j, i);
        }

        bencode_free(p);
      }

    } else {
      syslog(LOG_DEBUG, "failed to decode key 'path' in 'files' at index %ld", i);
    }


    bencode_free(t);
    bencode_free(b_file_dict);

    info->files[i] = infofile;
  }


  bencode_free(b_file_list);
}

ti_announce_t *
ti_parse_announce_url(char *announce)
{
  // protocol://hostname[:port][/request_string]
  ti_announce_t *ann = malloc(sizeof(ti_announce_t));
  memset(ann, 0, sizeof(ti_announce_t));
  ann->protocol = TRACKER_PROTOCOL_UNSUPPORTED;
  ann->port = 0;
  ann->hostname = NULL;
  ann->request_string = NULL;

  ann->url = malloc(strlen(announce) + 1);
  memcpy(ann->url, announce, strlen(announce));
  ann->url[strlen(announce)] = '\0';

  /* protocol */
  char *loc = strchr(announce, ':');
  if (!loc) {
    syslog(LOG_ERR, "ti_parse_announce_url: no hostname found for announce url");
    free(ann);
    return NULL;
  }

  if (memcmp(announce, "https", strlen("https") > (loc - announce) ? strlen("https") : (loc - announce)) == 0) {
    ann->protocol = TRACKER_PROTOCOL_HTTPS;
  } else if (memcmp(announce, "http", strlen("http") > (loc - announce) ? strlen("http") : (loc - announce)) == 0) {
    ann->protocol = TRACKER_PROTOCOL_HTTP;
  } else if (memcmp(announce, "udp", strlen("udp") > (loc - announce) ? strlen("udp") : (loc - announce)) == 0) {
    ann->protocol = TRACKER_PROTOCOL_UDP;
  } else {
    ann->protocol = TRACKER_PROTOCOL_UNSUPPORTED;
  }

  /* hostname */
  loc += 3; // get past the ://
  char *loc2 = strchr(loc, ':');
  size_t s = 0;

  if (loc2) {
    s = loc2 - loc;
    ann->hostname = malloc(s + 1);
    memcpy(ann->hostname, loc, s);
    ann->hostname[s] = '\0';

    /* port */
    loc2++; // get past the :
    sscanf(loc2, "%" SCNu16, &ann->port);
  }

  /* request_string
   * http://www.rasterbar.com/products/libtorrent/udp_tracker_protocol.html#request-string
   */

  char *loc3 = strchr(loc, '/');

  if (loc3) {
    /* hostname if there was no port */
    if (!loc2) {
      s = loc3 - loc;
      ann->hostname = malloc(s + 1);
      memcpy(ann->hostname, loc, s);
      ann->hostname[s] = '\0';
    }

    /* request string */
    ann->request_string = malloc(strlen(loc3) + 1);
    strncpy(ann->request_string, loc3, strlen(loc3));
    ann->request_string[strlen(loc3)] = '\0';
  }

  return ann;
}

void
ti_get_announce(torrent_info_t *ti, bencode_t *ben)
{
  char *announce = ti_get_string_value(ben, "announce");
  if (!announce) {
    ti->announce = NULL;
    return;
  }

  ti->announce = ti_parse_announce_url(announce);
  free(announce);
}

void
ti_get_announce_list(torrent_info_t *ti, bencode_t *ben)
{
  // announce-list is a list of list of strings where the inner list
  // has only 1 element which is a string. Essentially, it's a list of strings
  bencode_t *outer = bencode_new();

  if (bencode_dict_get(ben, "announce-list", outer) != 0 ||
      outer->type != BENCODE_LIST) {
    syslog(LOG_DEBUG, "failed to decode key 'announce-list' in torrent");
    bencode_free(outer);
    ti->announce_list = NULL;
    ti->announce_list_size = 0;
    return;
  }

  size_t current_announce_url = 0;

  ti->announce_list_size = 0;

  for (size_t i = 0; i < outer->length; i++) {
    bencode_t *inner = bencode_new();

    if (bencode_list_get(outer, i, inner) != 0 ||
        inner->type != BENCODE_LIST) {
      bencode_free(inner);
      continue;
    }

    ti->announce_list_size += inner->length;
    ti->announce_list = realloc(ti->announce_list, ti->announce_list_size * sizeof(ti_announce_t *));
    for (size_t j = 0; j < inner->length; j++) {
      bencode_t *ben_announce_url = bencode_new();

      if (bencode_list_get(inner, j, ben_announce_url) != 0 ||
          ben_announce_url->type != BENCODE_STR) {
        bencode_free(ben_announce_url);
        continue;
      }

      char *temp = malloc(ben_announce_url->length + 1);
      memset(temp, 0, ben_announce_url->length + 1);
      memcpy(temp, ben_announce_url->value.s, ben_announce_url->length);
      ti->announce_list[current_announce_url++] = ti_parse_announce_url(temp);
      free(temp);

      bencode_free(ben_announce_url);
    }

    bencode_free(inner);
  }


  bencode_free(outer);
}

void
ti_get_creation_date(torrent_info_t *ti, bencode_t *ben)
{
  // creation date :: int
  ti->creation_date = (int) ti_get_int_value(ben, "creation date");
}

void
ti_get_comment(torrent_info_t *ti, bencode_t *ben)
{
  ti->comment = ti_get_string_value(ben, "comment");
}

void
ti_get_created_by(torrent_info_t *ti, bencode_t *ben)
{
  ti->created_by = ti_get_string_value(ben, "created by");
}

void
ti_get_encoding(torrent_info_t *ti, bencode_t *ben)
{
  ti->encoding = ti_get_string_value(ben, "encoding");
}

void
ti_free(torrent_info_t *ti)
{
  if (!ti)
    return;

  if (ti->info_hash)
    free(ti->info_hash);

  if (ti->info) {
    if (ti->info->name)
      free(ti->info->name);

    if (ti->info->files) {
      for (int i = 0; i < ti->info->files_length; i++) {
        if (ti->info->files[i]->path) {
          for (int j = 0; j < ti->info->files[i]->path_length; j++) {
            free(ti->info->files[i]->path[j]);
          }
          free(ti->info->files[i]->path);
        }

        if (ti->info->files[i]->md5sum) {
          free(ti->info->files[i]->md5sum);
        }

        free(ti->info->files[i]);
      }
      free(ti->info->files);
    }

    if (ti->info->pieces)
      free(ti->info->pieces);

    free(ti->info);
  }

  if (ti->announce) {
    if (ti->announce->hostname)
      free(ti->announce->hostname);
    if (ti->announce->url)
      free(ti->announce->url);
    free(ti->announce);
  }

  if (ti->announce_list) {
    for (size_t i = 0; i < ti->announce_list_size; i++)
      if (ti->announce_list[i])
        free(ti->announce_list[i]);
    free(ti->announce_list);
  }

  if (ti->comment)
    free(ti->comment);

  if (ti->created_by)
    free(ti->created_by);

  if (ti->encoding)
    free(ti->encoding);

  free(ti);
}

