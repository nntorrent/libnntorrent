CFLAGS+=-c -Wall -std=c11 -g -fPIC -D_DEFAULT_SOURCE
INCLUDES+=-I../libbencode
LDFLAGS+=-L../libbencode -lbencode -Wl,-rpath,../libbencode -lcrypto -lm -lgmp -pthread -lcurl

SRCS=$(wildcard *.c)
#OBJS=$(addsuffix .o, $(basename $(SRCS)))
OBJS=metainfo.o peer.o tracker.o tracker_request.o tracker_response.o util.o bitfield.o test.o evqueue.o torrent.o
EXECUTABLE=test
LIBNAME=libnntorrent.so
LIB_LDFLAGS=-shared -Wl,-soname,$(LIBNAME)

ifeq ($(shell uname -s),FreeBSD)
	CC=clang
	OBJS+=platform/bsd/mainloop.o
	INCLUDES+=-I/usr/local/include
	LDFLAGS+=-L/usr/local/lib
else
	CC=gcc
	OBJS+=platform/linux/mainloop.o
endif

.SUFFIXES: .c

all: $(EXECUTABLE) $(LIBNAME)

test: $(OBJS) test.o
	$(CC) $^ $(LDFLAGS) -o $@

test_bitfield: bitfield.o test_bitfield.o
	$(CC) $^ $(LDFLAGS) -o $@

lib: $(LIBNAME)

$(LIBNAME): $(OBJS)
	$(CC) $^ $(LIB_LDFLAGS) -o $@

.c.o: $(SRCS)
	$(CC) $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	rm -rf $(OBJS) $(EXECUTABLE) $(LIBNAME)
