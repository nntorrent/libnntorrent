#ifndef TORRENT_INFO_H
#define TORRENT_INFO_H

#include <stdlib.h>
#include <openssl/sha.h>
#include <stdint.h>
#include <bencode.h>

#include "constants.h"

typedef struct {
  size_t      length;
  char        *md5sum;
  char        **path;
  size_t      path_length;
} ti_info_file_t;

typedef struct {
  torrent_file_mode_t type;
  size_t          piece_length;
  char            *pieces;
  size_t          pieces_length;
  int             is_private;
  char            *name;
  size_t          length;
  char            *md5sum;
  ti_info_file_t  **files;
  size_t          files_length;
} ti_info_t;

typedef struct {
  char                    *url;
  tracker_protocol_t      protocol;
  char                    *hostname;
  uint16_t                port;
  char                    *request_string;
} ti_announce_t;

typedef struct {
  ti_info_t     *info;
  unsigned char *info_hash;
  ti_announce_t *announce;
  ti_announce_t **announce_list;
  size_t        announce_list_size;
  int           creation_date;
  char          *comment;
  char          *created_by;
  char          *encoding;
} torrent_info_t;

torrent_info_t *ti_new();
void ti_free(torrent_info_t *);

torrent_info_t *ti_parse_torrent_file(const char *path);
torrent_info_t *ti_parse_torrent_content(unsigned char *content, size_t size);
torrent_info_t *ti_parse_magnet_uri(char *uri);

void ti_get_info(torrent_info_t *, bencode_t *);
void ti_get_info_hash(torrent_info_t *, bencode_t *);
void ti_get_info_single_file(ti_info_t *, bencode_t *);
void ti_get_info_multi_file (ti_info_t *, bencode_t *);

ti_announce_t *ti_parse_announce_url(char *);
void ti_get_announce(torrent_info_t *, bencode_t *);
void ti_get_announce_list(torrent_info_t *, bencode_t *);
void ti_get_creation_date(torrent_info_t *, bencode_t *);
void ti_get_comment(torrent_info_t *, bencode_t *);
void ti_get_created_by(torrent_info_t *, bencode_t *);
void ti_get_encoding(torrent_info_t *, bencode_t *);

char *ti_get_string_value(bencode_t *, char *key);
long long ti_get_int_value(bencode_t *, char *key);

#endif // TORRENT_INFO_H
