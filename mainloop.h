#ifndef MAINLOOP_H
#define MAINLOOP_H

#if defined(__FreeBSD__)
#include <sys/types.h>
#include <sys/event.h>
typedef struct kevent ml_event_t;
#else
#include <sys/epoll.h>
typedef struct epoll_event ml_event_t;
#endif

#include "peer.h"

enum ml_fd_event {
  ML_READ,
  ML_WRITE,
};

/* return fd */
int ml_create();
void ml_destroy(int loop);

/* return positive for number of events, negative for error
 * input ml_event* for event output
 * input size to specify the maximum size of ml_event* and size is modified to specify
 * the actual size of output ml_event*
 */
int ml_get_events(int loop, ml_event_t *, int size);

/* returns positive for id, negative for error */
int ml_create_timer(int loop, int id);
int ml_set_timer(int loop, int id, int timeout, bool oneshot);

/* returns positive for id, negative for error */
int ml_create_custom(int loop, int id);
int ml_notify_custom(int loop, int id);
int ml_destroy_custom(int loop, int id);

bool ml_event_timer(ml_event_t *, int id);
bool ml_event_custom(ml_event_t *, int id);
bool ml_event_read(ml_event_t *);
bool ml_event_write(ml_event_t *);
bool ml_event_eof(ml_event_t *);
int  ml_event_fd(ml_event_t *);
void *ml_event_data(ml_event_t *);

int ml_add_fd(int loop, int fd, enum ml_fd_event, void *data);
int ml_del_fd(int loop, int fd, enum ml_fd_event);

#endif // MAINLOOP_H
