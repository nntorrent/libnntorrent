#ifndef TORRENT_H
#define TORRENT_H

#include <gmp.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>

#include "peer.h"
#include "tracker.h"
#include "metainfo.h"
#include "bitfield.h"
#include "constants.h"

typedef struct {
  size_t start;
  size_t length;
  size_t end;
  int fd;
  unsigned char *data;
} file_t;

typedef struct {
  int index;
  int begin;
  int length;
  peer_t *peer;
} request_queue_item_t;

struct _torrent_t {
  /* id is the ascii representation of info hash */
  char *id;

  tracker_t *tr;
  torrent_info_t *metainfo;

  peer_t *first_peer;
  peer_t *last_peer;
  int peers_num;
  int last_peer_checked;

  // dimensions and sizes
  int piece_num; /* total number of pieces */
  int index_num; /* total number of indexes */
  int indexes_len; /* indexes within a piece */
  int request_size;

  int last_piece; /* last piece number */
  int last_piece_size;
  int last_index; /* last index number as the offset within the last piece */
  int last_index_size;

  mpz_t *indexfield;
  bitfield_t *bitfield;

  int left_requests;
  request_queue_item_t pending_requests_queue[PENDING_REQUESTS_QUEUE_MAX];
  int pending_requests_queue_size;

  file_t *destfiles;
  int destfiles_num;

  size_t total_length;

  bool done;
  bool do_hash_check;

  int loop_fd;
  int tracker_timer_id;
  int peer_timer_id;
  int selfpipe[2];

  bool quit;

  pthread_mutex_t peer_queue_mutex;

  // linked list of torrents
  struct _torrent_t *next;
};

typedef struct _torrent_t torrent_t;

void *nn_torrent_run(void *);

torrent_t *nn_torrent_new(const char *filename, const char *destdir);
void nn_torrent_free(torrent_t *);
void nn_torrent_calc_dimensions(torrent_t *);

int nn_torrent_add_peer(torrent_t *t, peer_t *);
int nn_torrent_del_peer(torrent_t *t, peer_t *);
int nn_torrent_discover_peers(torrent_t *t);
int nn_torrent_add_incoming_peer(torrent_t *t, peer_t *);

int nn_torrent_get_message(torrent_t *, peer_t *);
int nn_torrent_respond(torrent_t *, peer_t *);
bool nn_torrent_is_complete(torrent_t *);
bool nn_torrent_check_completion(torrent_t *);
void nn_torrent_shutdown(torrent_t *);
void nn_torrent_quit(torrent_t *);
int nn_torrent_shutdown_peer(torrent_t *, peer_t *);
int nn_torrent_update_tracker(torrent_t *);
int nn_torrent_update(torrent_t *);
void nn_torrent_wakeup(torrent_t *);

int nn_torrent_arm_tracker_timer(torrent_t *, int);
int nn_torrent_arm_peer_timer(torrent_t *, int);

int nn_torrent_request(torrent_t *, peer_t *);
int nn_torrent_handle_piece(torrent_t *, peer_t *, peer_message_t *);
int nn_torrent_handle_request(torrent_t *, peer_t *, peer_message_t *, unsigned char *);
int nn_torrent_announce_have(torrent_t *, int);

int nn_torrent_check_piece_integrity(torrent_t *, int);
int nn_torrent_check_full_hash(torrent_t *);

#endif // TORRENT_H
