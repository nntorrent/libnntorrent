#include "tracker_response.h"

#if defined(__FreeBSD__)
#include <sys/endian.h>
#else
#include <endian.h>
#endif

#include <stdlib.h>
#include <syslog.h>

tracker_response_t *
tracker_response_new()
{
  tracker_response_t *res = malloc(sizeof(tracker_response_t));
  res->action = TRACKER_ACTION_INVALID;
  res->transaction_id = 0;
  res->connection_id = 0;

  res->interval = 0;
  res->leechers = 0;
  res->seeders = 0;

  res->peers = NULL;
  res->peers_len = 0;

  res->strerr = NULL;

  return res;
}

int
tracker_response_parse(tracker_response_t *res, char *resp, size_t resplen)
{
  if (resplen < 8) {
    syslog(LOG_DEBUG, "could not parse response: action and/or transaction id missing");
    return -1;
  }

  memcpy(&res->action, resp, sizeof(res->action));
  res->action = be32toh(res->action);

  switch (res->action) {
    case TRACKER_ACTION_CONNECT:
      return tracker_response_parse_connect(res, resp, resplen);

    case TRACKER_ACTION_ANNOUNCE:
      return tracker_response_parse_announce(res, resp, resplen);

    case TRACKER_ACTION_ERROR:
      return tracker_response_parse_error(res, resp, resplen);

    default:
      syslog(LOG_DEBUG, "could not parse response: tracker sent unknown action %d", res->action);
      return -1;
  }

  return 0;
}

int
tracker_response_parse_announce(tracker_response_t *res, char *resp, size_t resplen)
{
  if (resplen < 20) {
    syslog(LOG_DEBUG, "could not parse announce response: response too short %ld", resplen);
    return -1;
  }

  memcpy(&res->action, resp, sizeof(res->action));
  res->action = be32toh(res->action);

  memcpy(&res->transaction_id, resp+4, sizeof(res->transaction_id));
  res->transaction_id = be32toh(res->transaction_id);

  memcpy(&res->interval, resp+8, sizeof(res->interval));
  res->interval = be32toh(res->interval);

  memcpy(&res->leechers, resp+12, sizeof(res->leechers));
  res->leechers = be32toh(res->leechers);

  memcpy(&res->seeders, resp+16, sizeof(res->seeders));
  res->seeders = be32toh(res->seeders);

  res->peers_len = (resplen - 20) / 6;

  if (res->peers) {
    free(res->peers);
  }

  res->peers = malloc(sizeof(tracker_peer_t) * res->peers_len);
  memset(res->peers, 0, sizeof(tracker_peer_t) * res->peers_len);

  for (int i = 0; i < res->peers_len; i++) {
    tracker_peer_t p;
    memcpy(&p.ipa,  resp+(20 + 6 * i), sizeof(p.ipa));
    memcpy(&p.port, resp+(24 + 6 * i), sizeof(p.port));

    res->peers[i] = (tracker_peer_t) {.ipa = p.ipa, .port = be16toh(p.port)};
  }

  return 0;
}

int
tracker_response_parse_connect(tracker_response_t *res, char *resp, size_t resplen)
{
  if (resplen < 16) {
    syslog(LOG_DEBUG, "could not parse connect response: response too short %ld", resplen);
    return -1;
  }

  memcpy(&res->action, resp, sizeof(res->action));
  res->action = be32toh(res->action);

  memcpy(&res->transaction_id, resp+4, sizeof(res->transaction_id));
  res->transaction_id = be32toh(res->transaction_id);

  memcpy(&res->connection_id, resp+8, sizeof(res->connection_id));

  return 0;
}

int
tracker_response_parse_error(tracker_response_t *res, char *resp, size_t resplen)
{
  if (resplen < 8) {
    syslog(LOG_DEBUG, "could not parse error response: response too short %ld", resplen);
    return -1;
  }

  memcpy(&res->action, resp, sizeof(res->action));
  res->action = be32toh(res->action);

  memcpy(&res->transaction_id, resp+4, sizeof(res->transaction_id));
  res->transaction_id = be32toh(res->transaction_id);

  if (res->strerr)
    free(res->strerr);

  res->strerr = malloc(resplen - 8);
  memcpy(res->strerr, resp + 8, resplen - 8);

  return 0;
}

void
tracker_response_free(tracker_response_t *resp)
{
  if (!resp)
    return;

  if (resp->peers)
    free(resp->peers);

  if (resp->strerr)
    free(resp->strerr);

  free(resp);
}
