#include "tracker_request.h"

#include <stdlib.h>
#include <string.h>

#if defined(__FreeBSD__)
#include <sys/endian.h>
#else
#include <endian.h>
#endif

#include "util.h"

tracker_request_t *
tracker_request_new()
{
  tracker_request_t *req = malloc(sizeof(tracker_request_t));
  
  memset(req->info_hash, 0, SHA_DIGEST_LENGTH);
  memset(req->peer_id, 0, PEER_ID_SIZE);

  req->action = TRACKER_ACTION_INVALID;

  req->uploaded = 0;
  req->downloaded = 0;
  req->left = 0;

  req->event = TRACKER_EVENT_NONE;
  req->numwant = 200;
  req->key = 0;

  req->request_string = NULL;
  req->transaction_id = 0;
  req->connection_id = 0;

  return req;
}

int
tracker_request_set_info(tracker_request_t *req, torrent_info_t *ti)
{
  memcpy(req->info_hash, ti->info_hash, SHA_DIGEST_LENGTH);
  if (req->request_string) {
    free(req->request_string);
    req->request_string = NULL;
  }

  if (ti->announce->request_string) {
    req->request_string = malloc(strlen(ti->announce->request_string) + 1);
    memcpy(req->request_string,
           ti->announce->request_string,
           strlen(ti->announce->request_string));
    req->request_string[strlen(ti->announce->request_string)] = '\0';
  }

  return 0;
}

int
tracker_request_gen_peer_id(unsigned char *res)
{
  /* https://wiki.theory.org/BitTorrentSpecification#peer_id */
  memcpy(res, "-NN0001-", 8);
  uint8_t t;
  for (size_t i = 8; i < PEER_ID_SIZE; i++) {
    t = rand() % 10 + 48; // random ascii numbers
    memcpy(res + i, &t, sizeof(uint8_t));
  }

  return 0;
}

char *
tracker_request_serialize(tracker_request_t *req, ssize_t *msglen)
{
  char *res;

  /* bittorrent udp spec: http://bittorrent.org/beps/bep_0015.html */
  /* connection_id: int64 0x41727101980 */
  if (req->action == TRACKER_ACTION_CONNECT) {
    char msg[16] = {0,0,0x4,0x17,0x27,0x10,0x19,0x80 /* int64 -> initial connection_id */
                   ,0,0,0,0                          /* int32 -> action: 0 for connect */
                   ,0,0,0,0};                        /* int64 -> random */
    req->transaction_id = gen_rand_32();
    int32_t tid = htobe32(req->transaction_id);
    memcpy(msg+12, &tid, sizeof(tid));

    *msglen = 16;
    res = malloc(*msglen);

    memcpy(res, msg, *msglen);

    return res;
  }

  /* announce request: http://bittorrent.org/beps/bep_0015.html
   * request_string extension: http://www.rasterbar.com/products/libtorrent/udp_tracker_protocol.html#request-string
   * option type of extensions: http://bittorrent.org/beps/bep_0041.html
   */
  int32_t action = htobe32(req->action);
  int32_t tid = htobe32(req->transaction_id);
  int64_t downloaded = htobe64(req->downloaded);
  int64_t left = htobe64(req->left);
  int64_t uploaded = htobe64(req->uploaded);
  int32_t event = htobe32(req->event);
  int32_t ip = 0;
  int32_t numwant = htobe32(req->numwant);
  int16_t port = htobe16(9000);
  int8_t urldata_option_type = 0x2;
  int8_t request_string_len = req->request_string ? strlen(req->request_string) : 0;

  *msglen = 100 + request_string_len;
  res = malloc(*msglen);
  memset(res, 0, *msglen);

  memcpy(res,     &req->connection_id,  sizeof(int64_t));
  memcpy(res+8,   &action,              sizeof(int32_t));
  memcpy(res+12,  &tid,                 sizeof(int32_t));
  memcpy(res+16,  req->info_hash,       SHA_DIGEST_LENGTH);
  memcpy(res+36,  req->peer_id,         PEER_ID_SIZE);
  memcpy(res+56,  &downloaded,          sizeof(int64_t));
  memcpy(res+64,  &left,                sizeof(int64_t));
  memcpy(res+72,  &uploaded,            sizeof(int64_t));
  memcpy(res+80,  &event,               sizeof(int32_t));
  memcpy(res+84,  &ip,                  sizeof(int32_t));
  memcpy(res+88,  &req->key,            sizeof(int32_t));
  memcpy(res+92,  &numwant,             sizeof(int32_t));
  memcpy(res+96,  &port,                sizeof(int16_t));
  memcpy(res+98,  &urldata_option_type, sizeof(int8_t));
  memcpy(res+99,  &request_string_len,  sizeof(int8_t));

  if (req->request_string) {
    memcpy(res+100, req->request_string,  strlen(req->request_string));
  }

  return res;
}

void
tracker_request_free(tracker_request_t *req)
{
  if (!req)
    return;

  if (req->request_string)
    free(req->request_string);

  free(req);
}

int
tracker_gen_peer_id(unsigned char *res)
{
  /* https://wiki.theory.org/BitTorrentSpecification#peer_id */
  memcpy(res, "-NN0001-", 8);
  uint8_t t;
  for (size_t i = 8; i < PEER_ID_SIZE; i++) {
    t = rand() % 10 + 48; // random ascii numbers
    memcpy(res + i, &t, sizeof(uint8_t));
  }

  return 0;
}
