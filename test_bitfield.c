#include <stdio.h>
#include <stdlib.h>

#include "bitfield.h"

void
test_bit(bitfield_t *b, int bit)
{
  if (bitfield_tstbit(b, bit))
    fprintf(stderr, "true\n");
  else
    fprintf(stderr, "false\n");
}

int
main()
{
  bitfield_t b;
  bitfield_init(&b, 25);

  size_t len;
  unsigned char *buf;

  buf = bitfield_export(&b, &len);
  for (int i = 0; i < len; i++)
    fprintf(stderr, "%02X ", buf[i]);
  fprintf(stderr, "\n");

  test_bit(&b, 2);
  test_bit(&b, 24);
  test_bit(&b, 10);
  test_bit(&b, 7);
  test_bit(&b, 5);
  test_bit(&b, 6);

  bitfield_setbit(&b, 12);
  bitfield_setbit(&b, 10);
  bitfield_setbit(&b, 2);
  bitfield_setbit(&b, 24);
  bitfield_setbit(&b, 7);
  bitfield_setbit(&b, 5);
  bitfield_setbit(&b, 6);

  buf = bitfield_export(&b, &len);
  for (int i = 0; i < len; i++)
    fprintf(stderr, "%02X ", buf[i]);
  fprintf(stderr, "\n");

  test_bit(&b, 2);
  test_bit(&b, 24);
  test_bit(&b, 10);
  test_bit(&b, 7);
  test_bit(&b, 5);
  test_bit(&b, 6);

  bitfield_clrbit(&b, 2);
  bitfield_clrbit(&b, 10);

  buf = bitfield_export(&b, &len);
  for (int i = 0; i < len; i++)
    fprintf(stderr, "%02X ", buf[i]);
  fprintf(stderr, "\n");

  test_bit(&b, 2);
  test_bit(&b, 24);
  test_bit(&b, 10);
  test_bit(&b, 7);
  test_bit(&b, 5);
  test_bit(&b, 6);

  buf = bitfield_export(&b, &len);
  for (int i = 0; i < len; i++)
    fprintf(stderr, "%02X ", buf[i]);
  fprintf(stderr, "\n");

  bitfield_clear(&b);
  return 0;
}
