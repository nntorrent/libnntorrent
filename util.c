#include "util.h"

#include <stdlib.h>

void
hex_print(char *prompt, unsigned char *t, size_t s) {
  fprintf(stderr, "%s: ", prompt);
  for (int i = 0; i < s; i++) {
    fprintf(stderr, "%.2x ", t[i]);
  }
  fprintf(stderr, "\n");
}

int32_t
gen_rand_32()
{
  return rand();
}
