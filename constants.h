#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <openssl/sha.h>

#define MAX_REQUEST_SIZE 16384 // 2^14
#define TORRENT_ID_SIZE 40
#define PENDING_REQUESTS_QUEUE_MAX 300

#define INFO_HASH_SIZE SHA_DIGEST_LENGTH

#define PEER_MAX_PENDING_REQUESTS 30
#define PEER_ID_SIZE 20

typedef enum {
  TRACKER_ACTION_INVALID = -1,
  TRACKER_ACTION_CONNECT = 0,
  TRACKER_ACTION_ANNOUNCE = 1,
  TRACKER_ACTION_SCRAPE = 2,
  TRACKER_ACTION_ERROR = 3
} tracker_action_t;

typedef enum {
  TRACKER_EVENT_NONE = 0,
  TRACKER_EVENT_COMPLETED = 1,
  TRACKER_EVENT_STARTED = 2,
  TRACKER_EVENT_STOPPED = 3,
} tracker_event_t;

typedef enum {
  TRACKER_PROTOCOL_HTTP,
  TRACKER_PROTOCOL_HTTPS,
  TRACKER_PROTOCOL_UDP,
  TRACKER_PROTOCOL_UNSUPPORTED
} tracker_protocol_t;

typedef enum {
  TORRENT_FILE_MODE_SINGLE,
  TORRENT_FILE_MODE_MULTI,
} torrent_file_mode_t;

#endif // CONSTANTS_H
