#ifndef EVQUEUE_H
#define EVQUEUE_H

#include <pthread.h>
#include "torrent.h"

enum evqueue_event_type {
  EVQUEUE_EVENT_NONE,
  EVQUEUE_EVENT_ADDED,
  EVQUEUE_EVENT_STARTED,
  EVQUEUE_EVENT_COMPLETED,
  EVQUEUE_EVENT_ERROR,
};

struct evqueue_event {
  char torrent_id[TORRENT_ID_SIZE];
  enum evqueue_event_type type;

  struct evqueue_event *next;
};

int evqueue_init();
int evqueue_destroy();

struct evqueue_event *evqueue_event_new(char *id, enum evqueue_event_type);
void evqueue_event_free(struct evqueue_event *);

int evqueue_enqueue(struct evqueue_event *);
struct evqueue_event *evqueue_dequeue();

int evqueue_len();
int evqueue_fd();

char *evqueue_type_to_str(enum evqueue_event_type);

#endif // EVQUEUE_H
