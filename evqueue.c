#include "evqueue.h"

#include <unistd.h>
#include <fcntl.h>
#include <syslog.h>

static pthread_mutex_t evqueue_mutex;
static struct evqueue_event *events;

static int queue_len;

static int sender;
static int receiver;

int
evqueue_init()
{
  events = NULL;
  queue_len = 0;

  int fds[2];
  if (pipe(fds) != 0) {
    syslog(LOG_DEBUG, "evqueue_init creating pipe: %m");
    return -1;
  }

  sender = fds[1];
  receiver = fds[0];

  /* make the pipe non-blocking for read/write since mainloop takes care of multiplexing */
  for (int i = 0; i < 2; i++) {
    int flags = 0;
    if ((flags = fcntl(fds[i], F_GETFL, 0)) < 0) {
      syslog(LOG_WARNING, "evqueue_init getting fds[%d] flags: %m", i);
    } else {
      flags |= O_NONBLOCK;
      if (fcntl(fds[i], F_SETFL, flags) != 0) {
        syslog(LOG_WARNING, "evqueue_init setting fds[%d] flags: %m", i);
      }
    }
  }

  pthread_mutex_init(&evqueue_mutex, NULL);

  return 0;
}

int
evqueue_destroy()
{
  if (queue_len > 0) {
    for (struct evqueue_event *e = evqueue_dequeue(); e; e = evqueue_dequeue()) {
      evqueue_event_free(e);
    }
  }

  close(sender);
  close(receiver);
  sender = -1;
  receiver = -1;
  queue_len = 0;

  pthread_mutex_destroy(&evqueue_mutex);

  return 0;
}

struct evqueue_event *
evqueue_event_new(char *tid, enum evqueue_event_type type)
{
  struct evqueue_event *e = malloc(sizeof(struct evqueue_event));
  memset(e, 0, sizeof(struct evqueue_event));
  memcpy(e->torrent_id, tid, TORRENT_ID_SIZE);
  e->type = type;
  e->next = NULL;

  return e;
}

void
evqueue_event_free(struct evqueue_event *e)
{
  if (e) {
    free(e);
  }
}


int
evqueue_enqueue(struct evqueue_event *event)
{
  pthread_mutex_lock(&evqueue_mutex);

  if (!events) {
    events = event;
  } else {
    struct evqueue_event *e;
    for (e = events; e->next; e = e->next);
    e->next = event;
  }

  queue_len++;

  char buf = 'x';
  if (write(sender, &buf, 1) != 1) {
    syslog(LOG_WARNING, "evqueue_enqueue: write failed: %m");
  }

  pthread_mutex_unlock(&evqueue_mutex);
  return 0;
}

struct evqueue_event *
evqueue_dequeue()
{
  if (!events || queue_len == 0) {
    return NULL;
  }

  char buf;
  if (read(receiver, &buf, 1) != 1) {
    syslog(LOG_WARNING, "evqueue_dequeue: read failed: %m");
    return NULL;
  }

  pthread_mutex_lock(&evqueue_mutex);

  struct evqueue_event *e = events;
  events = events->next;

  queue_len--;

  pthread_mutex_unlock(&evqueue_mutex);
  return e;
}

int
evqueue_fd()
{
  return receiver;
}

int
evqueue_len()
{
  return queue_len;
}

char *
evqueue_type_to_str(enum evqueue_event_type t)
{
  char temp[50];
  memset(temp, 0, 50);

  switch (t) {
    case EVQUEUE_EVENT_NONE:
      strcpy(temp, "none");
      break;

    case EVQUEUE_EVENT_ERROR:
      strcpy(temp, "error");
      break;

    case EVQUEUE_EVENT_ADDED:
      strcpy(temp, "added");
      break;

    case EVQUEUE_EVENT_STARTED:
      strcpy(temp, "started");
      break;

    case EVQUEUE_EVENT_COMPLETED:
      strcpy(temp, "completed");
      break;

    default:
      strcpy(temp, "unknown");
      break;
  }

  char *msg = malloc(strlen(temp) + 1);
  memset(msg, 0, strlen(temp) + 1);
  memcpy(msg, temp, strlen(temp));

  return msg;
}
