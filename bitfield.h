#ifndef BITFIELD_H
#define BITFIELD_H

#include <stdint.h>
#include <stdlib.h>

typedef struct {
  uint8_t *bits;
  size_t bitlen;
} bitfield_t;

bitfield_t *bitfield_new();
void bitfield_init(bitfield_t *, size_t size);
void bitfield_setbit(bitfield_t *, int);
int  bitfield_tstbit(bitfield_t *, int);
void bitfield_clrbit(bitfield_t *, int);
unsigned char *bitfield_export(bitfield_t *, size_t *);
void bitfield_free(bitfield_t *);

#endif // BITFIELD_H
