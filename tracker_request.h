#ifndef TRACKER_REQUEST_H
#define TRACKER_REQUEST_H

#include <stdint.h>
#include <sys/types.h>

#include "metainfo.h"
#include "constants.h"

typedef struct {
  tracker_action_t action;
  int32_t         transaction_id;
  int64_t         connection_id;

  unsigned char   info_hash[SHA_DIGEST_LENGTH];
  unsigned char   peer_id[PEER_ID_SIZE];

  uint64_t        uploaded;
  uint64_t        downloaded;
  uint64_t        left;

  tracker_event_t event;
  unsigned        numwant;
  int32_t         key;

  char            *request_string;
} tracker_request_t;

tracker_request_t *tracker_request_new();
int tracker_request_set_info(tracker_request_t *, torrent_info_t *);

int tracker_request_gen_peer_id(unsigned char *);
char *tracker_request_serialize(tracker_request_t *, ssize_t *);

void tracker_request_free(tracker_request_t *);

#endif // TRACKER_REQUEST_H
