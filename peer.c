#include "peer.h"

#if defined(__FreeBSD__)
#include <sys/endian.h>
#else
#include <endian.h>
#endif

#include <errno.h>
#include <unistd.h>
#include <syslog.h>

#define SEND_MESSAGE(SOCK, BUF, LEN) \
  if (send((SOCK), (BUF), (LEN), MSG_NOSIGNAL) != (LEN)) { \
    syslog(LOG_DEBUG, "could not send successfully: %m"); \
    free((BUF)); \
    return -1; \
  }

peer_t *
peer_new(int32_t ip,
         uint16_t port,
         int sock,
         unsigned char *info_hash,
         unsigned char *peer_id)
{
  peer_t *p = malloc(sizeof(peer_t));
  memset(p, 0, sizeof(peer_t));

  p->ip = ip;
  p->port = port;
  p->sock = sock;

  /* protocol: client connections start out as "choked" and "not interested" */
  p->state = 0;
  peer_set_state(p, PEER_AM_CHOKED);
  peer_set_state(p, PEER_CHOKED);

  p->expected_info_hash = NULL;
  p->expected_peer_id = NULL;

  if (info_hash) {
    p->expected_info_hash = malloc(INFO_HASH_SIZE);
    memcpy(p->expected_info_hash, info_hash, INFO_HASH_SIZE);
  }

  if (peer_id) {
    p->expected_peer_id = malloc(PEER_ID_SIZE);
    memcpy(p->expected_peer_id, peer_id, PEER_ID_SIZE);
  }

  p->msg_queue = NULL;
  p->pending_requests = 0;

  p->buffer = NULL;
  p->buffer_len = 0;
  p->expected_buffer_len = 0;

  memset(p->info_hash, 0, INFO_HASH_SIZE);
  memset(p->peer_id, 0, PEER_ID_SIZE);

  p->next = NULL;
  p->prev = NULL;

  return p;
}

int
peer_connect(peer_t *p)
{
  /* tcp connection to peer */
  struct addrinfo hints;
  struct addrinfo *ai;

  memset(&hints, 0, sizeof(hints));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  /* convert int32_t to dot notation */
  struct in_addr in;
  in.s_addr = p->ip;

  char port2[6];
  if (snprintf(port2, 6, "%d", p->port) < 0) {
    syslog(LOG_DEBUG, "peer_connect: could not convert %d to char *", p->port);
    return -1;
  }

  int ret;
  if ((ret = getaddrinfo(inet_ntoa(in), port2, &hints, &ai) != 0)) {
    syslog(LOG_DEBUG, "peer_connect: could not get address info: %s", gai_strerror(ret));
    return -1;
  }

  p->sock = socket(ai->ai_family, ai->ai_socktype | SOCK_NONBLOCK, ai->ai_protocol);

  if (connect(p->sock, ai->ai_addr, ai->ai_addrlen) == 0) {
    peer_set_state(p, PEER_CONNECTED);
  } else {
    if (errno == EINPROGRESS) {
      peer_set_state(p, PEER_CONNECTING);
    } else {
      syslog(LOG_DEBUG, "peer_new: cannot connect: %m");
      close(p->sock);
      freeaddrinfo(ai);
      return -1;
    }
  }

  freeaddrinfo(ai);
  return 0;
}

peer_t *
peer_new_incoming(int sock, unsigned char *info_hash, unsigned char *peer_id)
{
  peer_t *p = peer_new(0, 0, sock, info_hash, peer_id);
  peer_set_state(p, PEER_INCOMING | PEER_CONNECTED);
  return p;
}

peer_t *
peer_new_outgoing(int32_t ip,
                  uint16_t port,
                  unsigned char *info_hash,
                  unsigned char *peer_id)
{
  peer_t *p = peer_new(ip, port, 0, info_hash, peer_id);
  peer_set_state(p, PEER_OUTGOING);
  return p;
}

void
peer_free(peer_t *p) {
  if (!p) {
    return;
  }

  mpz_clear(p->bitfield);

  peer_message_t *m = peer_queue_pop(p);
  while (m) {
    peer_message_free(m);
    m = peer_queue_pop(p);
  }

  if (p->expected_info_hash)
    free(p->expected_info_hash);

  if (p->expected_peer_id)
    free(p->expected_peer_id);

  close(p->sock);
  free(p);
}

int
peer_send_handshake(peer_t *p, unsigned char *info_hash, unsigned char *peer_id)
{
  /* The handshake is a required message and must be the first message transmitted
   * by the client. It is (49+len(pstr)) bytes long.
   *
   * handshake: <pstrlen><pstr><reserved><info_hash><peer_id>
   *
   * In version 1.0 of the BitTorrent protocol, pstrlen = 19, and pstr = "BitTorrent protocol".
   */
  int pstrlen = 19;
  char *pstr = "BitTorrent protocol";

  unsigned char buf[49+pstrlen];
  buf[0] = pstrlen;
  memcpy(buf+1, pstr, pstrlen);
  memset(buf+1+pstrlen, 0, 8);
  memcpy(buf+1+pstrlen+8, info_hash, 20);
  memcpy(buf+1+pstrlen+28, peer_id, 20);

  if (send(p->sock, buf, 49+pstrlen, MSG_NOSIGNAL) != 49+pstrlen) {
    syslog(LOG_DEBUG, "could not send handshake: %m");
    return -1;
  }

  peer_set_state(p, PEER_HANDSHAKE_SENT);

  if (peer_state(p) & PEER_HANDSHAKE_RECEIVED) {
    peer_set_state(p, PEER_READY);
  }

  return 0;
}

int
peer_receive_handshake(peer_t *p)
{
  /* get pstrlen */
  uint8_t pstrlen;
  size_t templen = recv(p->sock, &pstrlen, 1, 0);
  if (templen != 1) {
    syslog(LOG_DEBUG, "could not receive pstrlen from sock %d: %m", p->sock);
    return -1;
  }

  /* get pstr */
  unsigned char pstr[pstrlen];
  templen = recv(p->sock, pstr, pstrlen, 0);
  if (templen != pstrlen) {
    syslog(LOG_DEBUG, "could not receive pstr from sock %d: %m", p->sock);
    return -1;
  }

  /* get reserved bytes (8 bytes) */
  unsigned char reserved_bytes[8];
  templen = recv(p->sock, reserved_bytes, 8, 0);
  if (templen != 8) {
    syslog(LOG_DEBUG, "could not receive reserved bytes from sock %d: %m", p->sock);
    return -1;
  }

  /* get info hash */
  templen = recv(p->sock, p->info_hash, INFO_HASH_SIZE, 0);
  if (templen != INFO_HASH_SIZE) {
    syslog(LOG_DEBUG, "could not receive info hash from sock %d: %m", p->sock);
    return -1;
  }

  /* get peer id */
  templen = recv(p->sock, p->peer_id, PEER_ID_SIZE, 0);
  if (templen != PEER_ID_SIZE) {
    syslog(LOG_DEBUG, "could not receive peer id from sock %d: %m", p->sock);
    return -1;
  }

  peer_set_state(p, PEER_HANDSHAKE_RECEIVED);

  if (peer_state(p) & PEER_HANDSHAKE_SENT) {
    peer_set_state(p, PEER_READY);
  }

  return 0;
}

int
peer_keepalive(peer_t *p) {
  /* keep-alive: <len=0000> */
  uint32_t buf = 0;

  if (send(p->sock, &buf, sizeof(uint32_t), MSG_NOSIGNAL) != sizeof(uint32_t)) {
    syslog(LOG_DEBUG, "peer_keepalive: send failed: %m");
    return -1;
  }

  return 0;
}

int
_peer_send_status(peer_t *p, PEER_MESSAGE_TYPE type, const char *debug_msg) {
  uint32_t res_len;
  unsigned char *buf = peer_make_msg(type, NULL, 0, &res_len);

  SEND_MESSAGE(p->sock, buf, res_len);

  free(buf);

  return 0;
}

int
_peer_send_extended(peer_t *p,
                    PEER_MESSAGE_TYPE type,
                    const char *debug_msg,
                    int32_t index,
                    int32_t begin,
                    unsigned char *block,
                    int32_t block_len)
{
  /* if block == NULL, the message is either request or cancel; otherwise, it's piece */
  uint32_t payload_len = sizeof(int32_t) * 2 + (block ? block_len : sizeof(int32_t));
  unsigned char payload[payload_len];

  int32_t i2 = htobe32(index);
  int32_t begin2 = htobe32(begin);
  int32_t block_len2 = htobe32(block_len);

  memcpy(payload, &i2, sizeof(int32_t));
  memcpy(payload + sizeof(int32_t), &begin2, sizeof(int32_t));

  if (block) {
    memcpy(payload + sizeof(int32_t) * 2, block, block_len);
  } else {
    memcpy(payload + sizeof(int32_t) * 2, &block_len2, sizeof(int32_t));
  }

  uint32_t res_len;
  unsigned char *buf = peer_make_msg(type, payload, payload_len, &res_len);

  SEND_MESSAGE(p->sock, buf, res_len);

  free(buf);

  return 0;
}

int
peer_choke(peer_t *p) {
  fprintf(stderr, "K");
  /* choke: <len=0001><id=0> */
  peer_set_state(p, PEER_CHOKED);
  return _peer_send_status(p, PEER_MESSAGE_CHOKE, "choke");
}

int
peer_unchoke(peer_t *p) {
  fprintf(stderr, "!K");
  /* unchoke: <len=0001><id=1> */
  p->state &= ~(PEER_CHOKED);
  return _peer_send_status(p, PEER_MESSAGE_UNCHOKE, "unchoke");
}

int
peer_interested(peer_t *p) {
  fprintf(stderr, "I");
  /* interested: <len=0001><id=2> */
  if (peer_state(p) & PEER_AM_INTERESTED) {
    return 0;
  }
  peer_set_state(p, PEER_AM_INTERESTED);
  return _peer_send_status(p, PEER_MESSAGE_INTERESTED, "interested");
}

int
peer_notinterested(peer_t *p) {
  fprintf(stderr, "!I");
  /* uninterested: <len=0001><id=3> */
  if (!(peer_state(p) & PEER_AM_INTERESTED)) {
    return 0;
  }
  p->state &= ~(PEER_AM_INTERESTED);
  return _peer_send_status(p, PEER_MESSAGE_NOTINTERESTED, "notinterested");
}

int
peer_have(peer_t *p, int32_t i) {
  fprintf(stderr, "H");
  if (mpz_tstbit(p->bitfield, i)) {
    /* Prevent sending have messages where the other peer is unlikely
     * to download them.
     *
     * More discussion here:
     * https://wiki.theory.org/BitTorrentSpecification#have:_.3Clen.3D0005.3E.3Cid.3D4.3E.3Cpiece_index.3E
     */
    return 0;
  }
  /* have have: <len=0005><id=4><piece index> */
  uint32_t payload_len = sizeof(int32_t);
  unsigned char payload[payload_len];
  int32_t i2 = htobe32(i);
  memcpy(payload, &i2, payload_len);

  uint32_t res_len;
  unsigned char *buf = peer_make_msg(PEER_MESSAGE_HAVE, payload, payload_len, &res_len);

  SEND_MESSAGE(p->sock, buf, res_len);

  free(buf);

  return 0;
}

int
peer_request(peer_t *p, int32_t i, int32_t begin, int32_t length) {
  /* request: <len=0013><id=6><index><begin><length> */
  fprintf(stderr, "R");
  int ret = _peer_send_extended(p, PEER_MESSAGE_REQUEST, "request", i, begin, NULL, length);
  if (ret == 0) {
    p->pending_requests++;
  }

  return ret;
}

int
peer_bitfield(peer_t *p, unsigned char *field, uint32_t len) {
  fprintf(stderr, "B");
  uint32_t res_len;
  unsigned char *buf = peer_make_msg(PEER_MESSAGE_BITFIELD, field, len, &res_len);

  SEND_MESSAGE(p->sock, buf, res_len);

  peer_set_state(p, PEER_BITFIELD_SENT);
  free(buf);

  return 0;
}

int
peer_piece(peer_t *p, int32_t i, int32_t begin, unsigned char *block, size_t block_len) {
  fprintf(stderr, "P");
  return _peer_send_extended(p, PEER_MESSAGE_PIECE, "piece", i, begin, block, block_len);
}

int
peer_cancel(peer_t *p, int32_t i, int32_t begin, int32_t block_len) {
  fprintf(stderr, "C");
  return _peer_send_extended(p, PEER_MESSAGE_CANCEL, "cancel", i, begin, NULL, block_len);
}

int
peer_port(peer_t *p, uint16_t port) {
  char buf[4];
  buf[0] = 3;
  buf[1] = 9;
  memcpy(buf+2, &port, sizeof(uint16_t));

  if (send(p->sock, buf, 14, MSG_NOSIGNAL) != 2) {
    syslog(LOG_DEBUG, "could not send successfully: %m");
    return -1;
  }

  return 0;
}

unsigned char *
peer_make_msg(PEER_MESSAGE_TYPE type, unsigned char *payload, uint32_t payload_len, uint32_t *res_len)
{
  *res_len = sizeof(int32_t) + 1 + payload_len;
  unsigned char *buf = malloc(*res_len);

  uint8_t id = (uint8_t) type;

  int32_t len = htobe32(payload_len + 1);
  memcpy(buf, &len, sizeof(int32_t));
  memcpy(buf + sizeof(int32_t), &id, 1);

  if (payload) {
    memcpy(buf + sizeof(int32_t) + 1, payload,  payload_len);
  }

  return buf;
}

int
peer_get_message(peer_t *p)
{
  ssize_t l;

  struct in_addr in;
  in.s_addr = p->ip;

  if (p->expected_buffer_len == 0) {
    /* new message coming in */
    int32_t len;

    /*
     * TODO MSG_WAITALL is to get around tcp sometimes segmenting the length
     * 32bit block
     *
     * eventually this has to be accommodated for non-blocking, but not today!
     */
    l = recv(p->sock, &len, sizeof(int32_t), MSG_WAITALL);

    if (l < sizeof(int32_t)) {
      syslog(LOG_WARNING, "couldn't get all of the length integer");
    }

    if (l < 0) {
      syslog(LOG_DEBUG, "peer_get_message: recv failed on sock %d: %m", p->sock);
      return -1;
    }

    if (l == 0) {
      syslog(LOG_DEBUG, "peer EOF");
      peer_message_t *m = peer_message_new(PEER_MESSAGE_SHUTDOWN);
      peer_queue_push(p, m);
      peer_message_free(m);
      return 0;
    }

    p->expected_buffer_len = be32toh(len);

    if (p->expected_buffer_len > 16394) {
      syslog(LOG_WARNING, "expecting the wrong number of bytes from %s", inet_ntoa(in));
      /*p->expected_buffer_len = 0;*/
      /*return -1;*/
    }

    if (p->expected_buffer_len == 0) {
      peer_message_t *m = peer_message_new(PEER_MESSAGE_NONE);
      peer_queue_push(p, m);
      peer_message_free(m);
      return 0;
    }

    p->buffer_len = 0;
    p->buffer = malloc(p->expected_buffer_len);
    memset(p->buffer, 0, p->expected_buffer_len);
  }


  l = recv(p->sock, p->buffer + p->buffer_len, p->expected_buffer_len - p->buffer_len, 0);


  if (l <= 0) {
    syslog(LOG_DEBUG, "peer_get_message: recv failed on sock %d: %m", p->sock);
    p->expected_buffer_len = 0;
    p->buffer_len = 0;
    free(p->buffer);
    return -1;
  }

  p->buffer_len += l;
  int ret = 0;

  if (p->buffer_len >= p->expected_buffer_len) {
    ret = peer_handle_message(p, p->buffer, p->buffer_len);
    p->expected_buffer_len = 0;
    p->buffer_len = 0;
    free(p->buffer);
  }

  return ret;
}

int
peer_handle_message(peer_t *p, unsigned char *buf, ssize_t len)
{
  if (len < 0) {
    syslog(LOG_DEBUG, "peer_handle_message: len < 0");
    return -1;
  }

  peer_message_t *msg = _parse_message(buf, len);
  if (!msg) {
    return -1;
  }

  // peer itself does some light processing of messages
  // but it should pass through torrent's processing before
  // being discarded
  int ret = _handle_message(p, msg);

  // message still needs to be processed by teh torrent
  peer_queue_push(p, msg);
  peer_message_free(msg);

  return ret;
}

peer_message_t *
_parse_message(unsigned char *buf, size_t len)
{
  peer_message_t *msg = peer_message_new(PEER_MESSAGE_NONE);

  if (len <= 0) {
    syslog(LOG_DEBUG, "_parse_message: len <= 0");
    return NULL;
  }

  memcpy(&msg->type, buf, 1);

  /*
   * for an odd reason, I have a sense of need for copious output
   * an attempt to balance my need and size of resulting log files
   * yields these fprintf's. Here lies the compromise I have been
   * willing to make for the sake of humanity.
   */

  switch(msg->type) {
    case PEER_MESSAGE_CHOKE:
      fprintf(stderr, "k");
      break;

    case PEER_MESSAGE_UNCHOKE:
      fprintf(stderr, "!k");
      break;

    case PEER_MESSAGE_INTERESTED:
      fprintf(stderr, "i");
      break;

    case PEER_MESSAGE_NOTINTERESTED:
      fprintf(stderr, "!i");
      break;

    case PEER_MESSAGE_HAVE:
      fprintf(stderr, "h");
      memcpy(&msg->index, buf+1, sizeof(uint32_t));
      msg->index = be32toh(msg->index);
      break;

    case PEER_MESSAGE_BITFIELD:
      fprintf(stderr, "b");
      msg->block_len = len - 1;
      msg->block = malloc(len - 1);
      memcpy(msg->block, buf + 1, msg->block_len);
      break;

    case PEER_MESSAGE_REQUEST:
      fprintf(stderr, "r");
      memcpy(&msg->index, buf + 1, sizeof(uint32_t));
      memcpy(&msg->begin, buf + 1 + sizeof(uint32_t), sizeof(uint32_t));
      memcpy(&msg->block_len, buf + 1 + (2*sizeof(uint32_t)), sizeof(uint32_t));

      msg->index = be32toh(msg->index);
      msg->begin = be32toh(msg->begin);
      msg->block_len = be32toh(msg->block_len);
      break;

    case PEER_MESSAGE_PIECE:
      fprintf(stderr, "p");
      memcpy(&msg->index, buf + 1, sizeof(uint32_t));
      memcpy(&msg->begin, buf + 1 + sizeof(uint32_t), sizeof(uint32_t));

      msg->index = be32toh(msg->index);
      msg->begin = be32toh(msg->begin);

      msg->block_len = len - 9;
      msg->block = malloc(msg->block_len);
      memcpy(msg->block, buf + 9, msg->block_len);
      break;

    default:
      fprintf(stderr, "x");
      peer_message_free(msg);
      return NULL;
  }

  return msg;
}

int
_handle_message(peer_t *p, peer_message_t *msg)
{
  switch(msg->type) {
    case PEER_MESSAGE_INVALID:
      syslog(LOG_DEBUG, "_handle_message: received invalid message");
      break;

    case PEER_MESSAGE_CHOKE:
      peer_set_state(p, PEER_AM_CHOKED);
      break;

    case PEER_MESSAGE_UNCHOKE:
      p->state &= ~PEER_AM_CHOKED;
      break;

    case PEER_MESSAGE_INTERESTED:
      peer_set_state(p, PEER_INTERESTED);
      break;

    case PEER_MESSAGE_NOTINTERESTED:
      p->state &= ~PEER_INTERESTED;
      break;

    case PEER_MESSAGE_HAVE:
      mpz_setbit(p->bitfield, msg->index);
      break;

    case PEER_MESSAGE_BITFIELD:
      mpz_init2(p->bitfield, msg->block_len * 8);
      mpz_import(p->bitfield, msg->block_len, -1, 1, 0, 0, msg->block);
      break;

    case PEER_MESSAGE_REQUEST:
      break;

    case PEER_MESSAGE_PIECE:
      p->pending_requests--;
      break;

    case PEER_MESSAGE_CANCEL:
      break;

    case PEER_MESSAGE_PORT:
      break;

    default:
      syslog(LOG_WARNING, "_handle_message: received unknown message type");
      break;
  }

  return 0;
}

peer_message_t *
peer_message_new(PEER_MESSAGE_TYPE type)
{
  peer_message_t *msg = malloc(sizeof(peer_message_t));
  memset(msg, 0, sizeof(peer_message_t));
  msg->type = type;
  return msg;
}

void
peer_message_free(peer_message_t *msg)
{
  if (!msg) {
    return;
  }

  if (msg->block) {
    free(msg->block);
  }

  free(msg);
}

void
peer_queue_push(peer_t *p, peer_message_t *m)
{
  peer_message_t *msg = p->msg_queue;

  if (msg) {
    for (; msg->next; msg = msg->next);
    msg->next = malloc(sizeof(peer_message_t));
    memset(msg->next, 0, sizeof(peer_message_t));

    memcpy(msg->next, m, sizeof(peer_message_t));

    if (m->block) {
      msg->next->block = malloc(m->block_len);
      memcpy(msg->next->block, m->block, m->block_len);
    }
  } else {
    p->msg_queue = malloc(sizeof(peer_message_t));
    memset(p->msg_queue, 0, sizeof(peer_message_t));

    memcpy(p->msg_queue, m, sizeof(peer_message_t));

    if (m->block) {
      p->msg_queue->block = malloc(m->block_len);
      memcpy(p->msg_queue->block, m->block, m->block_len);
    }
  }
}

peer_message_t *
peer_queue_pop(peer_t *p)
{
  peer_message_t *msg = p->msg_queue;

  if (!msg) {
    return NULL;
  }

  p->msg_queue = p->msg_queue->next;

  return msg;
}

int
peer_has_piece(peer_t *p, int piece)
{
  return mpz_tstbit(p->bitfield, piece);
}

PEER_STATE
peer_state(peer_t *p) {
  return p->state;
}

void
peer_set_state(peer_t *p, PEER_STATE s)
{
  p->state |= s;
}

void
peer_clear_state(peer_t *p)
{
  p->state = 0;
}
