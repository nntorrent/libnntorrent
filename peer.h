#ifndef PEER_CONNECTION_H
#define PEER_CONNECTION_H

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>
#include <math.h>
#include <openssl/sha.h>
#include <gmp.h>

#include "util.h"
#include "constants.h"

/* the following messages are bound to their protocol IDs
 * do not change unless the protocol spec changes
 */
typedef enum {
  PEER_MESSAGE_INVALID        = -1, // should be ignored, like keepalive or any other invalid message
  PEER_MESSAGE_CHOKE          = 0,
  PEER_MESSAGE_UNCHOKE        = 1,
  PEER_MESSAGE_INTERESTED     = 2,
  PEER_MESSAGE_NOTINTERESTED  = 3,
  PEER_MESSAGE_HAVE           = 4,
  PEER_MESSAGE_BITFIELD       = 5,
  PEER_MESSAGE_REQUEST        = 6,
  PEER_MESSAGE_PIECE          = 7,
  PEER_MESSAGE_CANCEL         = 8,
  PEER_MESSAGE_PORT           = 9,

  // reserved for future extensions and such
  PEER_MESSAGE_RESERVED1      = 10,
  PEER_MESSAGE_RESERVED2      = 11,
  PEER_MESSAGE_RESERVED3      = 12,
  PEER_MESSAGE_RESERVED4      = 13,
  PEER_MESSAGE_RESERVED5      = 14,

  // our own messages
  PEER_MESSAGE_NONE           = 15,
  PEER_MESSAGE_HANDSHAKE      = 16,
  PEER_MESSAGE_KEEPALIVE      = 17, // send peer a keepalive message
  PEER_MESSAGE_SHUTDOWN       = 18, // we're done with peer or it's not responding, shut it down
} PEER_MESSAGE_TYPE;

typedef enum {
  PEER_OUTGOING = 1,
  PEER_INCOMING = 2,

  PEER_CONNECTING = 4,
  PEER_CONNECTED = 8,

  PEER_HANDSHAKE_SENT = 16,
  PEER_HANDSHAKE_RECEIVED = 32,
  PEER_READY = 64,

  PEER_CHOKED = 128,
  PEER_AM_CHOKED = 256,

  PEER_INTERESTED = 512,
  PEER_AM_INTERESTED = 1024,

  PEER_BITFIELD_SENT = 2048,
} PEER_STATE;

typedef struct {
  unsigned char *content;
  uint32_t len;
} peer_raw_message_t;

struct _peer_message_t {
  PEER_MESSAGE_TYPE type;
  uint32_t index;
  uint32_t begin;
  uint32_t block_len;
  unsigned char *block;

  struct _peer_message_t *next;
};

typedef struct _peer_message_t peer_message_t;

struct _peer_t {
  int     sock;
  int32_t ip;
  uint16_t port;

  PEER_STATE state;

  mpz_t   bitfield;

  peer_message_t *msg_queue;
  int pending_requests;

  unsigned char *expected_info_hash;
  unsigned char *expected_peer_id;

  unsigned char *buffer;
  size_t buffer_len;
  size_t expected_buffer_len;

  char info_hash[INFO_HASH_SIZE];
  char peer_id[PEER_ID_SIZE];

  struct _peer_t *prev;
  struct _peer_t *next;
};

typedef struct _peer_t peer_t;

/* queueing */
void peer_queue_push(peer_t *, peer_message_t *);
peer_message_t *peer_queue_pop(peer_t *);

/* getting the connection going */
peer_t *peer_new(int32_t ip, uint16_t port, int sock, unsigned char *, unsigned char *);
void peer_free(peer_t *);

PEER_STATE peer_state(peer_t *);
void peer_set_state(peer_t *, PEER_STATE);
void peer_clear_state(peer_t *);

peer_t *peer_new_outgoing(int32_t ip,
                          uint16_t port,
                          unsigned char *info_hash,
                          unsigned char *peer_id);
peer_t *peer_new_incoming(int sock,
                          unsigned char *info_hash,
                          unsigned char *peer_id);

int peer_connect(peer_t *p);

int peer_send_handshake(peer_t *, unsigned char *, unsigned char *);
int peer_receive_handshake(peer_t *);

/*
 * handling messages
 */

int peer_get_message(peer_t *p);
int peer_handle_message(peer_t *p, unsigned char *buf, ssize_t len);

peer_message_t *peer_message_new(PEER_MESSAGE_TYPE);
void peer_message_free(peer_message_t *msg);
peer_message_t *_parse_message(unsigned char *buf, size_t len);
int _handle_message(peer_t *p, peer_message_t *msg);

/*
 * messages sent to peer
 */

int peer_keepalive(peer_t *);

/* status messages */
int peer_choke(peer_t *);
int peer_unchoke(peer_t *);
int peer_interested(peer_t *);
int peer_notinterested(peer_t *);

/* extended messages */
int peer_request(peer_t *, int32_t, int32_t, int32_t);
int peer_piece(peer_t *, int32_t, int32_t, unsigned char *, size_t);
int peer_cancel(peer_t *, int32_t, int32_t, int32_t);

/* other messages */
int peer_have(peer_t *, int32_t);
int peer_bitfield(peer_t *, unsigned char *, uint32_t);
int peer_port(peer_t *, uint16_t);

/* utility functions */
unsigned char *peer_make_msg(PEER_MESSAGE_TYPE type, unsigned char *payload, uint32_t payload_len, uint32_t *res_len);
int _peer_send_extended(peer_t *p,
                        PEER_MESSAGE_TYPE type,
                        const char *debug_msg,
                        int32_t index,
                        int32_t begin,
                        unsigned char *block,
                        int32_t block_len);

int peer_has_piece(peer_t *p, int piece);

#endif // PEER_CONNECTION_H
