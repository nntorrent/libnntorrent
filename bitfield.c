#include "bitfield.h"

#include <stdio.h>
#include <math.h>
#include <string.h>

/* for the purposes of bittorrent protocol, the bitfield should be
 * filled from right to left (highest bit is piece index 0)
 */

bitfield_t *
bitfield_new()
{
  bitfield_t *res = malloc(sizeof(bitfield_t));
  res->bits = NULL;
  res->bitlen = 0;
  return res;
}

void
bitfield_init(bitfield_t *b, size_t size)
{
  b->bitlen = size;
  b->bits = malloc((int) ceil(b->bitlen / 8.0));
  memset(b->bits, 0, (int) ceil(b->bitlen / 8.0));
}

void
bitfield_free(bitfield_t *b)
{
  if (b->bits) {
    free(b->bits);
  }
}

void
bitfield_setbit(bitfield_t *b, int bit)
{
  int bytenum = (int) floor(bit / 8.0);
  int offset = bit - bytenum * 8;
  b->bits[bytenum] |= (128 >> offset);
}

int
bitfield_tstbit(bitfield_t *b, int bit)
{
  int bytenum = (int) floor(bit / 8.0);
  int offset = bit - bytenum * 8;
  return b->bits[bytenum] & (128 >> offset);
}

void
bitfield_clrbit(bitfield_t *b, int bit)
{
  int bytenum = (int) floor(bit / 8.0);
  int offset = bit - bytenum * 8;
  b->bits[bytenum] &= ~(128 >> offset);
}

unsigned char *
bitfield_export(bitfield_t *b, size_t *len)
{
  *len = ceil(b->bitlen / 8.0);

  unsigned char *temp = malloc(*len);
  memcpy(temp, b->bits, *len);

  return temp;
}
