#ifndef TRACKER_RESPONSE_H
#define TRACKER_RESPONSE_H

#include <stdint.h>
#include <string.h>

#include "constants.h"

typedef struct {
  int32_t ipa;
  uint16_t port;
} tracker_peer_t;

typedef struct {
  tracker_action_t action;
  int32_t transaction_id;
  int64_t connection_id;

  char *strerr;

  int32_t interval;
  int32_t leechers;
  int32_t seeders;

  tracker_peer_t *peers;
  int     peers_len;

} tracker_response_t;

tracker_response_t *tracker_response_new();
void tracker_response_free(tracker_response_t *);

/* each of these functions can be used on its own */
int tracker_response_parse(tracker_response_t *, char *, size_t);
int tracker_response_parse_connect(tracker_response_t *, char *, size_t);
int tracker_response_parse_announce(tracker_response_t *, char *, size_t);
int tracker_response_parse_error(tracker_response_t *, char *, size_t);

#endif // TRACKER_RESPONSE_H
